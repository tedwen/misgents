/// <references path="typings/node/node.d.ts" />
import fs = require('fs')
// import Loader = require('./SchemaLoader')

(()=>{
    var sp = JSON.parse(fs.readFileSync('../.ssh/shards_production.json','utf8'))
    var pfx_c1s = ((dbs)=>{
        var dbm = {};
        for (var k in dbs){
            if (k == '#0')
                dbm['_'] = k;
            else {
                var c1 = k.codePointAt(1), c2 = k.codePointAt(2);
                for (var i=c1; i<=c2; i++){
                    dbm[String.fromCharCode(i)] = k
                }
            }
        }
        return dbm;
    })(sp.config.dbs);
    
    for (var sh of sp.users.shards) {
        var x = pfx_c1s[sh.match.pfx1.charAt(0)]
        sh.master.db = x || '#0'
        delete sh.match.field
    }
    //move field: username out
    sp.users.field = 'username'
    
    console.log(JSON.stringify(sp,null,4))
})()
