// generate SQL for table shardids and the stored procedure for shared ID generator
function gen_shardids_and_stored_proc(outdir) {
    var shardids = `
CREATE TABLE IF NOT EXISTS shardids (
    shard VARCHAR(4) NOT NULL PRIMARY KEY,
    shid INT NOT NULL,
    cap INT NOT NULL
);

DELIMITER //
CREATE PROCEDURE sharded_id
(
  IN sh VARCHAR(4),
  IN ini INT,
  IN mx INT
)
BEGIN
  INSERT INTO shardids VALUES(sh, ini, mx) ON DUPLICATE KEY UPDATE shid=shid+1;
  SELECT shid FROM shardids WHERE shard = sh;
END //
DELIMITER ;

DELIMITER //
CREATE TRIGGER check_cap BEFORE UPDATE ON shardids
FOR EACH ROW
BEGIN
  DECLARE msg VARCHAR(100);
  IF NEW.shid >= NEW.cap THEN
    SET msg = CONCAT('IDs used up for shard ', NEW.shard);
    SIGNAL SQLSTATE VALUE '45000'
      SET MESSAGE_TEXT = msg;
  END IF;
END //
DELIMITER ;
`
//CALL sharded_id('aa');
    fs.writeFileSync(`${outdir}/g_shardids.sql`, shardids)
}
gen_shardids_and_stored_proc('../rds')

