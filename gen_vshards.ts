/// <references path="typings/node/node.d.ts" />
import fs = require('fs')
import Loader = require('./SchemaLoader')

const PFX_CHARS = 'abcdefghijklmnopqrstuvwxyz'
const GRPSZ = 1048576 //0x100000
const RDS = 'misauth.cialuohgnbng.eu-west-1.rds.amazonaws.com:3306'
const LHOST = 'misauth_db_1:3306'
const DBNAME =  'misers'
const SCHEMA_DIR = __dirname + '/../schemas/';
const OUTPUT_DIR = __dirname + '/../.ssh/';

var g_host = LHOST

function gen_initial() {
    return {
    "default": {
        "master": {
            "host": g_host,
            "db": DBNAME
        }
    },
    "users": {
        "shards": [
            {
                "match": {
                    "field": "username", "pfx1": "!!", "pfx2": "a!",
                    "id1": 0xaffff, "id2": 0x100000
                },
                "master": {
                    "host": g_host, "db": `${DBNAME}_00`, "sh": "00"
                },
                "slaves": [
                    {
                        "host": g_host, "db": `${DBNAME}_00_r0`
                    }
                ]
            }
        ]
    }
};
}

function add_shard(shards, field, pfx1, pfx2, id1) {
    var sfx = pfx1.replace('!','0'), 
        dbn = sfx <= 'aa' ? DBNAME+'_'+sfx : DBNAME
    var sh = {
        match: {
            field: field,
            pfx1: pfx1, pfx2: pfx2,
            id1: id1, id2: id1 + GRPSZ
        },
        master: {
            host: g_host, db: dbn, sh: sfx
        }
    }
    shards.push(sh)
}

function gen_double_prefix(outdir, field='username') {
    var pfx1 = 'a!', id1 = 0x100000;
    var data = gen_initial();
    var shards = data.users.shards;
    for (var i=0; i<PFX_CHARS.length; i++) {
		for (var j=0; j<PFX_CHARS.length; j++) {
            var pfx2 = PFX_CHARS[i] + PFX_CHARS[j]
            add_shard(shards, field, pfx1, pfx2, id1)
            id1 += GRPSZ
            pfx1 = pfx2
        }
        //if [j] == z add z .. [i+1]!
        if (i+1 < PFX_CHARS.length) {
            var p1 = PFX_CHARS[i] + PFX_CHARS[PFX_CHARS.length-1],
                p2 = PFX_CHARS[i+1] + '!' 
            add_shard(shards, field, p1, p2, id1)
            id1 += GRPSZ
            pfx1 = p2
        }
    }
    add_shard(shards, field, 'zz', '~~', id1)
	//fs.writeFileSync(`${outdir}/shards_test.json`, JSON.stringify(data, null, 4))
    return(data)
}

// var basic = gen_double_prefix('../.ssh');

class ShardsBuilder extends Loader.SchemaEntity {
    
    constructor(schema_dir, schema_filename) {
        super(schema_dir, schema_filename)
    }

    findUserFk() {
        if (this.foreignKeys.length > 0) {
            for (var prop of this.foreignKeys) {
                if (prop.foreignKey == 'users(`id`)') {
                    return prop;
                }
            }
        }
        return null;
    }
    
    generateShardx(prop: Loader.SchemaProperty) {
        var ss = /(\w+)\(`(\w+)`\)/.exec(prop.foreignKey)
        if (ss.length < 3) {
            console.log('prop wrong:',prop.foreignKey)
            return {}
        }
        return {
            shardx: {
                field: `${prop.columnName}`,
                tablex: ss[1],
                fieldx: ss[2]
            }
        }
    }
}

function generateFkTables(mode: string) {
    var ls = new Loader.SchemaFiles(SCHEMA_DIR)
    var tables = {}
    ls.traverse(function(schema_filename){
        var schema = new ShardsBuilder(SCHEMA_DIR, schema_filename);
        var prop = schema.findUserFk()
        if (prop) {
            tables[schema.tableName] = schema.generateShardx(prop)
        }
    })
    return tables;
}

(()=>{
    var n = process.argv.length, mode = process.argv[n-1]=='live' ? 'live' : 'test'
    if (mode == 'live') g_host = RDS
    var basic = gen_double_prefix(mode)
    var fks = generateFkTables(mode)
    for (var k in fks) {
        basic[k] = fks[k]
    }
    fs.writeFileSync(`${OUTPUT_DIR}/shards_${mode}.json`, JSON.stringify(basic, null, 4))
})()
