/* global process, __dirname */
/// <reference path="typings/node/node.d.ts" />
import fs = require('fs');
import Loader = require('./SchemaLoader');

const PROJECT_NAME = process.argv[2] || '',
	  PARENT_DIR = __dirname.substring(0, __dirname.lastIndexOf('/')),
	  PROJECT_DIR = PROJECT_NAME ? PARENT_DIR + '/' + PROJECT_NAME : PARENT_DIR,
	  SCHEMA_DIR = PROJECT_DIR + '/schemas/',
	  OUTPUT = PROJECT_DIR + '/api/';
// const SCHEMA_DIR = __dirname + '/../schemas/';
// const OUTPUT = __dirname + '/../api/';

var HANDLERS_1 = `package api

import (
	"fmt"
	"github.com/make-it-social/papi/v2/dbo"
)

const (
	gExistByProvider = "%s AND EXISTS (SELECT id FROM managers WHERE provider_id=%s AND user_id=%s)"
	gExistByProduct = "%s AND EXISTS (SELECT a.provider_id FROM products a, managers b WHERE a.id=%s AND b.provider_id=a.provider_id AND b.user_id=%s)"
	providerIDinManager = "%s AND provider_id IN (SELECT provider_id FROM managers WHERE user_id=%s)"
	productIDinManager = "%s AND product_id IN (SELECT id FROM products WHERE provider_id IN (SELECT provider_id FROM managers WHERE user_id=%s))"
)
`;

var ROUTER_1 = `package api

import (
	"github.com/gocraft/web"
)

//SetRoutes - set API endpoint handlers
func SetRoutes(r *web.Router) {
`;


/**
 * FilesWriter for creating, saving g_router.go and go_handlers.go in /api
 * ROUTER_1 and HANDLERS_1 are saved at the beginning of these files.
 */
class FilesWriter {
	output_dir: string;
	routerFilename: string;
	router_file: any;
	handlersFilename: string;
	handlers_file: any;
	
	constructor(outdir) {
		this.output_dir = outdir
		this.createFiles()
	}
	
	writeRouter(...args: string[]) {
		var data: any = args.join('');
		fs.writeSync(this.router_file, data, null, null, null);
	}
	
	writeHandlers(...args: string[]) {
		var data: any = args.join('');
		fs.writeSync(this.handlers_file, data, null, null, null);
	}
	
	createFiles() {
		this.routerFilename = this.output_dir + 'g_router.go';
		this.router_file = fs.openSync(this.routerFilename, 'w');
		this.handlersFilename = this.output_dir + 'g_handlers.go';
		this.handlers_file = fs.openSync(this.handlersFilename, 'w');
		this.writeRouter(ROUTER_1)
		this.writeHandlers(HANDLERS_1)
	}

	closeRouter() {
		var data: any = "\n}\n"
		fs.writeSync(this.router_file, data, null, null, null)
		fs.closeSync(this.router_file)
	}

	closeFiles() {
		this.closeRouter()
		fs.closeSync(this.handlers_file)
	}
}


/**
 * Generator
 */
class Generator extends Loader.SchemaEntity {
	writers: FilesWriter;
	
	constructor(schema_dir, schema_filename, writers) {
		super(schema_dir, schema_filename);
		console.log('schema_dir=',schema_dir,'schema_filename=',schema_filename)
		this.writers = writers;
	}
	
	getCompoundKeys0(want_type=false) {
		var fknames = []
		for (var fk of this.compoundPrimaryKeys) {
			if (want_type) {
				fknames.push(fk)
			} else {
				fknames.push(fk.columnName)
			}
		}
		return fknames
	}
    getEntityNameFromId(id) {
        return id.replace('_id','s');   //assume adding s for plural
    }
    getCompoundKeyNames(want_type=false) {
        var fknames = []
        // skip first which is either owner_id, user_id by JWT or app_id by Auth
        for (var i=1; i<this.compoundPrimaryKeys.length; i++){
            var fk = this.compoundPrimaryKeys[i];
            if (i > 1) {
                var ename = this.getEntityNameFromId(fk.columnName)
                fknames.push(ename)
            }
            fknames.push(want_type ? fk : ':'+fk.columnName)
        }
        return fknames
    }
	
    // update: owner_id or first user_id will be implied in JWT, so not necessary in the path
	genCompoundKeyRoute(routes, rname) {
		var fknames = this.getCompoundKeyNames()
		var s = `r_${rname}.GET("/${fknames.join('/')}", WrapCall(Get${this.structName}Cpk))`
		routes.push(s)
	}

	getAuthHandler(name) {
		var _auxfs = {
			user: 'AuthUser',
			jwt: 'AuthUser',
			manager: 'AuthManager',
			super: 'AuthAdmin',
			admin: 'AuthAdmin',
			apikey: 'WrapCall',
			public: 'WrapCall',
			other: 'WrapCall'
		};
		return _auxfs[name];
	}

	getAuth() {
		//in schema: _routes:{ auth1: 'manager', autha: 'super', authu: 'manager', auth:'manager'}
		
		var rts = this.routes || {};
		var get1auth = rts['auth1'] || rts['auth'] || 'other';
		var getsauth = rts['autha'] || rts['auth'] || 'manager';
		var modauth = rts['authu'] || rts['auth'] || 'manager';
		return {
			getOne: this.getAuthHandler(get1auth), 
			getAll: this.getAuthHandler(getsauth), 
			modauth: this.getAuthHandler(modauth)
		};
	}

	checkForeignKeys(eps, authm) {
		var buf = [];
		for (var p of this.properties) {
			if (p.columnName == 'provider_id') {
				buf.push(`r.Get("/providers/:pid/${this.tableName}", ${authm}(query${this.structName}sByProvider))`)
			} else if (p.columnName == 'product_id') {
				buf.push(`r.Get("/products/:pid/${this.tableName}", ${authm}(query${this.structName}sByProduct))`)
			}
		}
		if (buf.length > 0) {
			eps['geta'] = buf.join('\n    ')
		}
	}
	genRoutes() {
		var auth = this.getAuth();
		var eps = {
			geta: `r.Get("/${this.tableName}", ${auth.getAll}(query${this.structName}s))`,
			get1: `r.Get("/${this.tableName}/:id", ${auth.getOne}(query${this.structName}))`,
			post: `r.Post("/${this.tableName}", ${auth.modauth}(create${this.structName}))`,
			put: `r.Put("/${this.tableName}/:id", ${auth.modauth}(update${this.structName}))`,
			del: `r.Delete("/${this.tableName}/:id", AuthAdmin(delete${this.structName}))`
		};
		this.checkForeignKeys(eps, auth.getAll)
		var es = ['geta','get1','post','put','del'], rts = this.routes || {}, excluded = rts['exclude'] || [];
		//schema: _routes:{exclude:['geta','get1','post','all','mod']}
		if (excluded.length>0) {
			if (excluded.indexOf('all')>-1) {
				es = [];
			} else if (excluded.indexOf('mod')>-1) {
				es = ['geta','get1'];
			} else {
				var buf = [];
				for (var x of es) {
					if (excluded.indexOf(x)<0) buf.push(x)
				}
				es = buf;
			}
		}
		var inlines = [];
		for (var e of es) {
			inlines.push(eps[e]);
		}
		//fk like /providers/:pid/products  -- replaced by checkForeignKeys above
/*		for (var p of this.foreignKeys) {
			if (excluded.indexOf(p.columnName)>-1) continue;
			var ss = /^(\w+)\(`(\w+)`\)/.exec(p.foreignKey), tn = ss[1], id = ss[2];
			inlines.push(`r.Get("/${tn}/:pid/${this.tableName}",${auth.getAll}(query${this.structName}s))`)
		}*/
		for (var r in rts) {
			if (r.charAt(0)=='/') {
				console.log('-----------',r)
				var ex = rts[r], methods = ex['methods'], handlers = ex['handlers'], aut = ex['auth'] || rts['auth'] || 'manager';
				for (var i in methods) {
					var ms = this.capitalise2(methods[i]), handler = handlers[i];
					inlines.push(`r.${ms}("${r}",${this.getAuthHandler(aut)}(${handler}))`);
				}
			}
		}
		return `
	//${this.tableName} - ${this.structName}
	${inlines.join('\n    ')}
`;
	}
	
	generateRouter() {
		this.writers.writeRouter(this.genRoutes())
	}
	
	//// handler generator:

	genHandlerGetOne() {
		return `
//GET /${this.tableName}/:id
func query${this.structName}(c *Context) {
	dp := &dbo.Database{}
	obj, err := dp.Get${this.structName}ByIds(c.GetParam("id"))
	if err != nil {
		c.ServerError(err)
		return
	}
	c.WriteJSON(obj)
}
`;
	}
	
	genHandlerGetAll() {
		var codes = [], tname = this.tableName, pname = this.pluralName, sname = this.structName;
		var genBy = function(p, ename, byname) {
			return `
//GET /${ename}/:pid/${tname}?offset=&limit=
func query${pname}By${byname}(c *Context) {
	pid := c.GetParam("pid")
	if pid == "" {
		pid = c.GetQuery("pid", "")
		if pid == "" {
			c.Error(400, "${byname}ID not specified")
			return
		}
	}
	where := fmt.Sprintf("${p.columnName}=%s", pid)
	if !c.IsAdmin() {
		where = fmt.Sprintf(gExistBy${byname}, where, pid, c.GetUserID())
	}
	offset := c.GetInt("offset", 0)
	limit := c.GetInt("limit", 500)
	dp := &dbo.Database{}
	es, err := dp.GetAll${sname}s(where, "", offset, limit)
	if err != nil {
		c.ServerError(err)
		return
	}
	c.WriteJSON(es)
}
`
		};
		var count = 0, hasUserID = false;
		for (var p of this.properties) {
			if (p.columnName == 'provider_id') {
				codes.push(genBy(p, 'providers', 'Provider'));
				count ++;
			} else if (p.columnName == 'product_id') {
				codes.push(genBy(p, 'products', 'Product'));
				count ++;
			} else if (p.columnName == 'user_id') {
				hasUserID = true;
			}
		}
		if (count == 0) {
			return this.genHandlerGetAll1(hasUserID);
		}
		return codes.join('\n')
	}

	genHandlerGetAll1(hasUserID) {
		var cond = '';
		if (this.tableName == 'providers') {
			cond = 'id IN (SELECT provider_id FROM managers WHERE user_id=%s)'
		} else {
			cond = 'user_id=%s'
		}
		return `
//GET /${this.tableName}?offset=&limit=
func query${this.pluralName}(c *Context) {
    dp := &dbo.Database{}
	where := ""
	if !c.IsAdmin() {
		where = fmt.Sprintf("${cond}", c.GetUserID())
	}
	offset := c.GetInt("offset", 0)
	limit := c.GetInt("limit", 500)
	es, err := dp.GetAll${this.structName}s(where, "", offset, limit)
	if err != nil {
		c.ServerError(err)
		return
	}
	c.WriteJSON(es)
}
`;
	}
	
	/**
	 * POST create new entity
	 */
	genHandlerPostNew() {
        var prefill = ''
        if (this.hasCompoundPrimaryKey) {
            var fks = this.compoundPrimaryKeys, cs = []
            for (var i=0; i<fks.length-1; i++){
                var fk = fks[i], kn = fk.columnName, ts = fk.dataType=='int'?'Int':''
                var gv = this.getGetValueByField(kn, ts)
                cs.push(`data["${kn}"] = ${gv}`)
            }
            prefill = `\n    ${cs.join('\n    ')}`
        }
		var code = `
//POST /${this.tableName}
func create${this.structName}(c *Context) {
	var jsbody map[string]interface{}
	err := c.GetBodyJSON(&jsbody)
	if err != nil {
		c.ServerError(err)
		return
	}
	dp := &dbo.Database{}
	n, er := dp.PostNew${this.structName}(jsbody)
	if er != nil {
		c.ServerError(er)
		return
	}
	c.WriteCreated(n)
}
`;
        return code
	}
	
    getDelPassword() {
        return this.tableName=='users' ? '\n        delete(data, "password")\n        delete(data, "password2")' : ''
    }
    
	byManager() {
		var cond = '', hasProviderId = false, hasProductId = false, hasUserId = false;
		var fmanager = '(SELECT provider_id FROM managers WHERE user_id=%s)'
		if (this.tableName == 'providers') {
			cond = `"%s AND id in ${fmanager}"`
		} else {
			for (var p of this.properties) {
				if (p.columnName == 'provider_id') {
					hasProviderId = true;
					break;
				} else if (p.columnName == 'product_id') {
					hasProductId = true;
					break;
				} else if (p.columnName == 'user_id') {
					hasUserId = true;
				}
			}
			if (hasProviderId) {
				cond = 'providerIDinManager'
			} else if (hasProductId) {
				cond = 'productIDinManager'
			} else {
				cond = `"%s AND user_id=%s"`
			}
		}
		return cond;
	}

	genHandlerPut() {
		var cond = this.byManager();
		return `
//PUT /${this.tableName}/:id
func update${this.structName}(c *Context) {
    id := c.GetParam("id")
	where := fmt.Sprintf("id=%s", id)
	if !c.IsAdmin() {
		where = fmt.Sprintf(${cond}, where, c.GetUserID())
	}
    var jsbody map[string]interface{}
    err := c.GetBodyJSON(&jsbody)
    if err != nil {
        c.ServerError(err)
        return
    }
    dp := &dbo.Database{}
    n, er := dp.Put${this.structName}ByWhere(where, jsbody)
    if er != nil {
        c.ServerError(er)
        return
    }
    c.WriteUpdated(n)
}
`;
	}
	
    getGetValueByField(cname, want_type='Int') {
        var s = want_type=='Int' ? '' : 's', gu = `c.GetUserId${s}()`
        var rv = {app_id: `c.GetAppId${s}()`, owner_id: gu, user_id: gu}[cname]
        return rv || `c.Param${want_type}("${cname}")`
    }
    
	genHandlerDelete() {
		return `
//DELETE /${this.tableName}/:id (only by admin)
func delete${this.structName}(c *Context) {
    id := c.GetParam("id")
    dp := &dbo.Database{}
    n, er := dp.DeleteAppByIds(id)
    if er != nil {
        c.ServerError(er)
        return
    }
    c.WriteDeleted(n)
}
`;
	}
	
	generateHandlers() {
		if (this.hasId) {
			this.writers.writeHandlers(this.genHandlerGetOne())
		}
		this.writers.writeHandlers(this.genHandlerGetAll())
		this.writers.writeHandlers(this.genHandlerPostNew())
		if (this.hasId) {
			this.writers.writeHandlers(this.genHandlerPut())
		}
        this.writers.writeHandlers(this.genHandlerDelete())
	}
	
	generate() {
		this.generateRouter()
		this.generateHandlers()
	}
}

/**
 * ApiBuilder
 */
class ApiBuilder {
	writers: FilesWriter;
	
	constructor(outdir) {
		this.writers = new FilesWriter(outdir);
	}
	
	run() {
		new Loader.SchemaFiles(SCHEMA_DIR).traverse((schema_filename)=>{
			new Generator(SCHEMA_DIR, schema_filename, this.writers).generate();
		});
		this.writers.closeFiles();
	}
}

new ApiBuilder(OUTPUT).run()
