/// <reference path="typings/node/node.d.ts"/>

import fs = require('fs')

/**
 * GenPostUsers
 * Generate sh scripts to curl post users
 * e.g.
 * curl -X POST -d '{"username":"aa1@b.c",..}' http://localhost:3000/v1/users -H "Content-Type:application/json"
 */
class GenPostUsers {
	sh_filename: string
	token_filename: string
	user_count: number
	
	constructor(shf, tkf: string, uc: number) {
		this.sh_filename = shf
		this.token_filename = tkf
		this.user_count = uc
	}
	
	generate(){}
}

const OUTPUT_DIR = '../.sh/',
	SH_FILENAME = 'post_users.sh',
	TK_FILENAME = 'temp_token',
	USER_COUNT = 10000;

(()=>{
	var shf = OUTPUT_DIR + SH_FILENAME
	var tkf = OUTPUT_DIR + TK_FILENAME
	var uc = USER_COUNT
	new GenPostUsers(shf, tkf, uc).generate()
})()
