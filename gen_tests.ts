/* global __dirname */

/// <reference path="typings/node/node.d.ts" />

import fs = require('fs');

/**
 * Generate test data for all data models in rds, and test codes following the codes in dbo.
 */
const GEN_COUNT = 5;
const VOWELS = ['a','e','i','o','u'];
const VOWELS2 = ['ea','au','ao','ou','ee','ia','ai','ua','ie','ei','iou'];
const SCONS = 'bcdfghjklmnpqrstvwxyz';
const PRCONS = ['br','bl','pr','qu','cr','wr','st','sc','sh','ch'];

function gen_integers(v, count) {
	var vs = [], mn = 0, mx = v._fk ? 5 : 100;
	for (var i=0; i<count; i++) {
		vs.push(Math.floor(Math.random() * mx));
	}
	return vs;
}

function random(container) {
	var x = Math.floor(Math.random() * container.length);
	if (typeof(container)=='string'){
		return container.charAt(x)
	}
	return container[x];
}

function gen_word(syllables) {
	var mx = Math.max(1, syllables || 4), ss = [];
	for (var i=0; i<mx; i++){
		var s = (i == 0 && Math.random() > 0.7) ? random(PRCONS) : random(SCONS); 
		ss.push(s);
		var s2 = Math.random() > 0.3 ? random(VOWELS) : random(VOWELS2);
		ss.push(s2);
	}
	if (Math.random() > 0.3) {
		ss.push(random(SCONS));
	}
	return ss.join(''); 
}

function gen_email() {
	return gen_word() + '@' + gen_word() + '.com';
}

function gen_uri() {
	return 'http://www.'+gen_word()+'.com';
}

function gen_strings(v, count) {
	var vs = [];
	for (var i=0; i<count; i++){
		var s = '';
		if (v.format == 'email') {
			s = gen_email();
		} else if (v.format == 'uri') {
			s = gen_uri();
		} else if (v.maxLength) {
			s = gen_word(10);
			if (s.length > v.maxLength) {
				s = s.substr(0, v.maxLength-1);
			}
		}
		vs.push(s);
	}
	return vs;
}

function save_data(data, fod, tname) {
	var columns = [], outdata = [];
	for (var k in data) {
		columns.push(k);
	}
	tname = '`'+tname+'`';
	var cs = '`'+columns.join('`,`')+'`';
	var lines = [];
	for (var i=0; i<GEN_COUNT; i++) {
		var vds = [];
		for (var k of columns) {
			var d = data[k][i], ds = typeof(d)=='string' ? `'${d}'` : d;
			vds.push(ds);
		}
		var vs = vds.join(',');
		var sql = `INSERT INTO ${tname}(${cs}) VALUES(${vs});`
		lines.push(sql);
	}
	fs.writeSync(fod, lines.join('\n') + '\n');
}

function shuffle(ar) {
	for (var i=0; i<ar.length; i++){
		var j = Math.floor(Math.random() * ar.length);
		if (i != j) {
			var t = ar[i];
			ar[i] = ar[j];
			ar[j] = t;
		}
	}
	return ar;
}

const PASSWRDS = [
	'pbkdf2_sha256$10000$y5ucb8mbjlri$acw4eDySVcdzMtaU9gVW1uVCiWWd96crDKpuz8g+FIo=',
	'pbkdf2_sha256$10000$4ts7gcwe40jd$8SgiA2IKJLqiQrySowLSjIX9yvCaFFOlyD6k+OoeiSg=',
	'pbkdf2_sha256$10000$4zn1kqkk6mqx$LG0ynfen1TnBOr/sNNmb4f80zqFkO3KJxRgeNZFWfVs=',
	'pbkdf2_sha256$10000$nwqnjzafwn2n$Wo3cvypH4CvZdVR1g/cmJ4FSDfHppqMsBZe5QhC0zxI=',
	'pbkdf2_sha256$10000$ezv89nj43g7j$Ox2HBAxjrXpaAh4jNXX+wd1ZS4nmPAGxaITkvCY+Onc='
];

function gen_insert_data(tableName, props, required, fod) {
	var data = {};
	for (var k in props) {
		var v = props[k];
		if (k == 'id' || k == 'created_at') continue;
		//ignore non-required fields
		if (required.indexOf(k) >= 0){
			switch (v.type){
				case 'integer':
					if (v._fk) {
						data[k] = shuffle([1,2,3,4,5]);
					} else {
						data[k] = gen_integers(v, GEN_COUNT);
					}
					break;
				case 'string':
					data[k] = gen_strings(v, GEN_COUNT);
					break;
				default:
					console.log(k,'type is',v.type);
			}
		}
	}
	if (tableName == 'users') {
		data['password'] = []
		for (var i=0; i<GEN_COUNT; i++){
			data['password'].push(PASSWRDS[Math.floor(Math.random() * PASSWRDS.length)])
		}
	}
	//console.log('save..',data);
	save_data(data, fod, tableName);
}

function schemaName(s) {
	s = s.replace('g_','');
	return s.charAt(0).toUpperCase() + s.substr(1);
}

/**
 * Generate ../rds/_init.sql for loading initial dummy data rows into tables.
 */
function gen_test_data(schema_dir, rds_dir, outdir){
	var fodname = rds_dir + 'g_init.sql', 
		fod = fs.openSync(fodname, 'w');
	fs.writeSync(fod, 'source g_dropper.sql;\nsource g_loader.sql;\n');
	for (var f of fs.readFileSync(`${rds_dir}g_loader.sql`,'utf8').split('\n')) {
		if (!/.sql/.test(f)) continue;
		var fn = schemaName(/(\w+).sql/.exec(f)[1]);
		var obj = JSON.parse(fs.readFileSync(`${schema_dir}${fn}.json`,'utf8'));
		if (obj.$schema && obj.properties) {
			let tableName = fn.replace('.json','').toLowerCase();
			gen_insert_data(tableName, obj.properties, obj.required, fod);
		}
	}
	fs.closeSync(fod);
}

/**
 * TODO
 * Generate ../tests/routes.go
 */
function gen_unit_test(){}

const SCHEMA_DIR = '../schemas/';
const RDS_DIR = '../rds/';
const OUT_DIR = '../tests/';

gen_test_data(SCHEMA_DIR, RDS_DIR, OUT_DIR);
