/// <references path="typings/node/node.d.ts" />
import fs = require('fs')
import Loader = require('./SchemaLoader')

class SchemaFile extends Loader.SchemaEntity {
    constructor(schema_dir, schema_filename) {
        super(schema_dir, schema_filename)
    }
    
    cleaned() {
        var oschema = {}
        for (var k in this.schema) {
            if (k == 'properties') {
                oschema['properties'] = {}
                for (var p in this.schema[k]) {
                    if (p.indexOf('_')!=0) {
                        oschema['properties'][p] = this.schema[k][p]
                    }
                }
            } else if (k != '$schema' && k != 'required' && k != 'additionalProperties') {
                oschema[k] = this.schema[k]
            }
        }
        return oschema
    }
    
    endpoints(paths) {
        var c = '/' + this.tableName,
            s = c + '/{id}'
        paths[c] = {
            get: {
                description: `Return all ${this.tableName}`,
                responses: {
                    '200': {
                        description: `A list of ${this.tableName}`,
                        schema: {
                            title: this.singularName,
                            type: 'array',
                            items: {
                                '$ref': `#/definitions/${this.singularName}`
                            }
                        }
                    }
                },
                security: [
                    {
                        basicAuth: []
                    }
                ]
            },
            post: {
                parameters: [
                    {
                        name: this.singularise(this.singularName),
                        in: 'body',
                        description: `Create a new ${this.singularName}`,
                        schema: {
                            '$ref': `#/definitions/${this.singularName}`
                        },
                        required: true
                    }
                ],
                responses: {
                    '201': {
                        description: `A new ${this.singularName} created`
                    }
                },
                security: [
                    {
                        basicAuth: []
                    }
                ]
            }
        }
        paths[s] = {
            get: {
                parameters: [
                    {
                        name: 'id',
                        in: 'path',
                        type: 'string',
                        description: 'ID',
                        required: true
                    }
                ],
                responses: {
                    '200': {
                        description: `Return ${this.singularName} by ID`
                    }
                },
                security: [
                    {
                        basicAuth: []
                    }
                ]
            },
            put: {
                parameters: [
                    {
                        in: 'path',
                        name: 'id',
                        required: true,
                        type: 'string'
                    },
                    {
                        in: 'body',
                        name: 'body',
                        description: `Update ${this.singularName}`,
                        required: true,
                        schema: {
                            '$ref': `#/definitions/${this.singularName}`
                        }
                    }
                ],
                responses: {
                    '200': {
                        description: 'Updated '+this.singularName
                    },
                    '404': {
                        description: this.singularName + ' not found'
                    }
                },
                security: [
                    {
                        basicAuth: []
                    }
                ]
            }
        }
    }
}

const SCHEMA_DIR = __dirname + '/../schemas/'
const TITLE = 'MakeitSocial Grouping API'
const DESC = 'Make it Social Grouping API'
const TERMS = 'https://makeitsocial.com/terms'
const VERSION = '0.1.0'

const ENTITIES = ['users','extras','clients','apps','circles','members','follows','managers']

/**
 * Swagger
 */
class Swagger {
    outdir: string
    swagger: Object
    
    constructor(outdir) {
        this.outdir = outdir
        this.swagger = {
            swagger: '2.0',
            info: {
                title: TITLE,
                description: DESC,
                termsOfService: TERMS,
                version: VERSION,
                contact: {
                    name: 'MakeitSocial',
                    url: 'https://makeitsocial.com',
                    email: 'dev@makeitsocial.com'
                }
            },
            schemes: 'https',
            consumes: ['application/json'],
            produces: ['application/json'],
            securityDefinitions: {
                basicAuth: {
                    type: 'basic',
                    description: 'HTTP Basic Auth with AppId and Secret'
                },
                jwt: {
                    type: 'JWT',
                    description: 'Token authentication with JWT'
                }
            },
            paths: {},
            definitions: {}
        }
    }
    
    run() {
        new Loader.SchemaFiles(SCHEMA_DIR).traverse((schema_filename)=>{
            //get dataset, remove _ props
            var s = new SchemaFile(SCHEMA_DIR, schema_filename)
            if (ENTITIES.indexOf(s.tableName) < 0) {
                return
            }
            this.swagger['definitions'][s.singularName] = s.cleaned()
            s.endpoints(this.swagger['paths'])
        })
        //save this.swagger file
        fs.writeFileSync(this.outdir+'swagger.json', JSON.stringify(this.swagger,null,4))
        //console.log(JSON.stringify(this.swagger,null,4))
    }
}

const OUT_DIR = __dirname + '/../public/'

new Swagger(OUT_DIR).run()
