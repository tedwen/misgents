/// <references path="typings/node/node.d.ts" />
import fs = require('fs')

// node gen_setup_sql.js <develop|production>
(()=>{
    var arg = process.argv[2] || 'develop'
    var data = JSON.parse(fs.readFileSync(`../.ssh/shards_${arg}.json`,'utf8'))
    var dbs = data.config.dbs
    //console.log(`CREATE USER 'MakeitSocial'@'%' IDENTIFIED BY 'test';`)
    for (var k in dbs) {
        var db = dbs[k]
        console.log(`CREATE DATABASE IF NOT EXISTS ${db} CHARACTER SET 'utf8';`)
        console.log(`GRANT ALL ON ${db}.* TO 'MakeitSocial';`)
        console.log(`USE ${db};`)
        console.log(`SOURCE g_loader.sql;`)
        if (k == '#0') {
            console.log(`SOURCE shardids.sql;`)
            console.log('SOURCE g_dataset.sql;')
        }
        console.log('')
    }
})()
