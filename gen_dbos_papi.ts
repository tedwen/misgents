/// <reference path="typings/node/node.d.ts" />
import fs = require('fs');
import Loader = require('./SchemaLoader');

const PROJECT_NAME = process.argv[2] || '',
	  PARENT_DIR = __dirname.substring(0, __dirname.lastIndexOf('/')),
	  PROJECT_DIR = PROJECT_NAME ? PARENT_DIR + '/' + PROJECT_NAME : PARENT_DIR,
	  SCHEMA_DIR = PROJECT_DIR + '/schemas/',
	  OUTPUT_DIR = PROJECT_DIR + '/dbo/';

const HEADER_T = `package dbo

import (
	"fmt"
	"bytes"
	"strings"
`;

/**
 * StructBuilder
 */
class StructBuilder extends Loader.SchemaEntity {
	output_dir: string;
	output_filename: string;
	output_file: any;
	
	constructor(schema_dir, schema_filename, outdir) {
		super(schema_dir, schema_filename);
		this.output_dir = outdir;
		this.output_filename = `${outdir}g_${this.tableName}.go`;
	}
	
	writeStrings(...args: string[]) {
		var data: any = args.join('');
		fs.writeSync(this.output_file, data, null, null, null);
	}
	
	writeHeader() {
		var imps = [];
		for (var k in this.imports) {
			var v = this.imports[k];
			if (k != 'zero')
				imps.push(`\t"${v}"`);
			else
				imps.push('\t"github.com/gocraft/dbr"')
		}
		this.writeStrings(HEADER_T, imps.join('\n'), '\n)\n');
	}
	
	writeStruct() {
		var fields = this.properties;
		var lines = ['','type '+this.structName+' struct {'];
		for (var i=0; i<fields.length; i++){
			var fld = fields[i], 
				dt = fld.dataType.indexOf('zero.')>=0?fld.dataType.replace('zero.','dbr.Null'):fld.dataType,
				ss = ['\t', fld.capitalName, this.alignLeft(fld.capitalName), dt];
			if (dt == 'dbr.NullInt') ss[3] = dt + '64';
			lines.push(ss.join(''));
		}
		lines.push('}\n\n');
		this.writeStrings(lines.join('\n'));
	}
	
	genJsonMarshaler() {
		var lines = [];
		var fields = this.properties;
		for (var i=0; i<fields.length; i++){
			var fld = fields[i]
			if (fld.jsonTag == '`json:"-"`') continue
			if (fld.dataType.indexOf('zero.') == 0) {
				var dtype = fld.dataType.substr(5);
				if (dtype == 'Time') dtype = 'Time.Format("2006-01-02T15:04:05.000Z")'
				lines.push(`\tif m.${fld.capitalName}.Valid {`)
				if (i > 0) {lines.push('\t\tbuf.WriteString(",")')}
				lines.push('\t\tbuf.WriteString(`"'+fld.columnName+'":`)')
				if (dtype == 'Int') {
					lines.push(`\t\tbuf.WriteString(fmt.Sprintf("%d",m.${fld.capitalName}.Int64))`)
				} else {
					lines.push('\t\tbuf.WriteString(`"`)')
					lines.push(`\t\tbuf.WriteString(m.${fld.capitalName}.${dtype})`)
					lines.push('\t\tbuf.WriteString(`"`)')
				}
				lines.push('\t}')
			} else {
				if (i > 0) {lines.push('\tbuf.WriteString(",")')}
				lines.push('\tbuf.WriteString(`"'+fld.columnName+'":`)')
				if (fld.dataType == 'int') {
					lines.push(`\tbuf.WriteString(strconv.Itoa(m.${fld.capitalName}))`)
				} else  {
					lines.push('\tbuf.WriteString(`"`)')
					if (fld.dataType == 'time.Time') {
						lines.push(`\tbuf.WriteString(m.${fld.capitalName}.Format("2006-01-02T15:04:05.000Z"))`)
					} else {
						lines.push(`\tbuf.WriteString(m.${fld.capitalName})`)
					}
					lines.push('\tbuf.WriteString(`"`)')
				}
			}
		}
		var code = `
func (m ${this.structName}) MarshalJSON() ([]byte, error) {
	var buf bytes.Buffer
	buf.WriteString("{")
${lines.join('\n')}
	buf.WriteString("}")
	return buf.Bytes(), nil
}
`;
		this.writeStrings(code)
	}
	
	genRowSource() {
		var cs = this.columnNames();
		var ps = this.varcharMap();
		var lines = `
const columns${this.structName} = "${cs.join(',')}"
var cmap${this.structName} = map[string]int{${ps}}
`;
		this.writeStrings(lines);
	}
	
	genGetByIds() {
		var name = this.structName;
		var tname = this.tableName;
		var lines = `
//Get${name}ByIds - query ${name} by ID as string
func (dp *Database) Get${name}ByIds(ids string) (*${this.structName}, error) {
	var e ${this.structName}
	err := dp.selectByInt("${this.tableName}", columns${this.structName}, "id", ids).LoadStruct(&e)
	return &e, err
}

//Get${name}ById - query ${name} by ID
func (dp *Database) Get${name}ById(id int) (interface{}, error) {
	var e ${this.structName}
	err := dp.selectByInt("${this.tableName}", columns${this.structName}, "id", strconv.Itoa(id)).LoadStruct(&e)
	return &e, err
}
`;
		this.writeStrings(lines);
	}
	
	genGetByCPk() {
		if (!this.hasCompoundPrimaryKey) return;
		var name = this.structName;
		var tname = this.tableName;
		var fknames = [], fktypes = [], ps = [], whs = [];
		for (var fk of this.compoundPrimaryKeys) {
			fknames.push(fk.columnName);
			fktypes.push(fk.dataType);
			ps.push(fk.columnName+' '+fk.dataType);
			whs.push(fk.columnName+'=?');
		}
		var pks = ps.join(','), where = whs.join(' AND '), 
		qss = fknames.join(',');
		var lines = `
func (dp *Database) Get${name}ByCPk(${pks}) ([]*${name}, error) {
	var es []*${name}
	_, err := dp.selectWithWhere("${tname}",columns${name},"${where}","").LoadStructs(&es)
	if err != nil {
		dbLogger.Errorf("%v", err)
		return 0, err
	}
	return es, err
}
`;
		this.writeStrings(lines);
	}
	
	genGetAll(){
		var name = this.structName;
		var tname = this.tableName;
		var cname = this.pluralName;
		var lines = `
//GetAll${cname} - query all ${cname} for a manager or admin
//where: WHERE clause
//order: after ORDER BY
//offset, limit: for pagination
func (dp *Database) GetAll${cname}(where, order string, offset, limit int) ([]*${name}, error) {
	var es []*${name}
	if order == "" && strings.HasSuffix(columns${name},"created_at") {
		order = "created_at DESC"
	}
	_, err := dp.selectWithWherePage("${tname}", columns${name}, where, order, offset, limit).LoadStructs(&es)
	return es, err
}
`;
		this.writeStrings(lines);
	}
	
	genGetByFk(fk: string) {
		var name= this.structName;
		var tname = this.tableName;
		var fkname = this.capitalise(fk.replace('_id','Id'));
		var fname = name + 'sBy' + fkname;
		var lines = `
//Get${fname} - query ${name} by foreign key ${fkname}
func (dp *Database) Get${fname}(id int) ([]*${name}, error) {
	var es []*${name}
	_, err := dp.selectByInt("${tname}", columns${name}, "${fk}", strconv.Itoa(id)).LoadStructs(&es)
	return es, err
}

//Get${fname}s - query ${name} by foreign key ${fkname} as ID string
func (dp *Database) Get${fname}s(ids string) ([]*${name}, error) {
	var es []*${name}
	_, err := dp.selectByInt("${tname}", columns${name}, "${fk}", ids).LoadStructs(&es)
	return es, err
}
`;
		this.writeStrings(lines);
	}
	
	genGetByIndex(x) {
		var stype = (x.dataType == "int") ? "Int" : "String";
		var code = `
//Get${this.structName}By${x.capitalName} - query ${this.structName}s by index ${x.columnName}
func (dp *Database) Get${this.structName}sBy${x.capitalName}(s string) ([]*${this.structName}, error) {
	var es []*${this.structName}
	_, err := dp.selectBy${stype}("${this.tableName}", columns${this.structName}, "${x.columnName}", s).LoadStructs(&es)
	return es, err
}
`;
			this.writeStrings(code);
	}

	getGenerator() {
		var gs = [];
		for (var p of this.properties) {
			if (p.generator) {
				var ps = /rs.(\d+)/.exec(p.generator); //rs 16
				gs.push(`cols = append(cols, "${p.columnName}")`);
				gs.push(`vals = append(vals, RandString(${ps[1]}))`);
			}
		}
		return gs.join('\n    ');
	}
	
	genInsert() {
		var name = this.structName, tname = this.tableName;
		var fldmaps = this.makeFieldsMap();
		var varinits = this.getGenerator();
		var lines = `
//PostNew${name} - insert new ${name} record from JSON body
func (dp *Database) PostNew${name}(body map[string]interface{}) (int64, error) {
	cols, vals := dp.buildInsertParams(body, cmap${this.structName})
	${varinits}
	sess := dp.getSession(true)
	res, err := sess.InsertInto("${this.tableName}").Columns(cols...).Values(vals...).Exec()
	if err != nil {
		dbLogger.Errorf("INSERT %+v failed:%v", body, err)
		return 0, err
	}
	id, er := res.LastInsertId()
	if er != nil {
		dbLogger.Errorf("INSERT %+v failed:%v", body, er)
	} else {
		body["id"] = id
		Signal("${tname}", "insert", "", body, id)
	}
	return id, er
}
`;
		this.writeStrings(lines);
	}
	
	genUpdateWhere() {
		var fldmaps = this.makeFieldsMap(true)
		var code = `
//Put${this.structName}ByWhere - update ${this.structName} record with JSON in body
func (dp *Database) Put${this.structName}ByWhere(where string, body map[string]interface{}) (int64, error) {
	sess := dp.getSession(true)
	updateMap := make(map[string]interface{}, 0)
	for k, v := range body {
		if _, exists := cmap${this.structName}[k]; exists {
			updateMap[k] = v
		} else {
			dbLogger.Infof("Unknown field: %s:%v", k, v)
		}
	}
	res, err := sess.Update("${this.tableName}").SetMap(updateMap).Where(where).Exec()
	if err != nil {
		dbLogger.Errorf("UPDATE %+v failed:%v", body, err)
		return 0, err
	}
	rows, err := res.RowsAffected()
	if err == nil {
		Signal("${this.tableName}", "update", where, body, rows)
	}
	return rows, err
}
`
		this.writeStrings(code)
	}
	
	genUpdate() {
		var fldmaps = this.makeFieldsMap(true);
		var code = `
//Put${this.structName}ById - update ${this.structName} record by ID with JSON body
func (dp *Database) Put${this.structName}ById(id int, body map[string]interface{}) (int64, error) {
	return dp.Put${this.structName}ByWhere(fmt.Sprintf("id = %d", id), body)
}

//Put${this.structName}ByIds - update ${this.structName} record by ID as string with JSON body
func (dp *Database) Put${this.structName}ByIds(ids string, body map[string]interface{}) (int64, error) {
	return dp.Put${this.structName}ByWhere(fmt.Sprintf("id = %s", ids), body)
}
`;
		this.writeStrings(code);
	}
	
	genUpdateByCpk() {
		var fldmaps = this.makeFieldsMap()
		var code = `
func (dp *Database) Put${this.structName}ByCpk(pks, body map[string]interface{}) (int64, error) {
	fields := map[string]int{${fldmaps}}
	query, params, err := db.BuildUpdateQueryPlus("${this.tableName}", pks, body, fields)
	if err != nil {
		return 0, err
	}
	rs, err := db.DoExec(query, params...)
	if err != nil {
		log.Printf("Update.erro=%v for %v\\n", err, query)
	}
	if rs <= 0 {
		log.Printf("Update.rowsAffected=%v for %v\\n", rs, query)
	}
	return rs, err
}
`
		this.writeStrings(code)
	}
	
	genDelete() {
		var code = `
//Delete${this.structName}ById - delete ${this.structName} record by ID
func (dp *Database) Delete${this.structName}ById(id int) (int64, error) {
	sess := dp.getSession(true)
	res, err := sess.DeleteFrom("${this.tableName}").Where("id = ?", id).Exec()
	if err != nil {
		dbLogger.Error()
		return 0, err
	}
	rows, err := res.RowsAffected()
	if err == nil {
		Signal("${this.tableName}", "delete", fmt.Sprintf("id=%d",id), nil, rows)
	}
	return rows, err
}

//Delete${this.structName}ByIds - delete ${this.structName} record by ID as string
func (dp *Database) Delete${this.structName}ByIds(ids string) (int64, error) {
	id, err := strconv.Atoi(ids)
	if err != nil {
		return 0, err
	}
	return dp.Delete${this.structName}ById(id)
}
`;
		this.writeStrings(code);
	}
    
    genDeleteByCpk() {
		var fknames = [], fktypes = [], ps = [], whs = [];
		for (var fk of this.compoundPrimaryKeys) {
			fknames.push(fk.columnName);
			fktypes.push(fk.dataType);
			ps.push(fk.columnName+' '+fk.dataType);
			whs.push(fk.columnName+'=?');
		}
		var pks = ps.join(','), where = whs.join(' AND '), 
		qss = fknames.join(',');
        var code = `
//Delete${this.structName}ByCpk - delete by foreign key ${pks}
func (dp *Database) Delete${this.structName}ByCpk(${pks}) (int64, error) {
	sess := dp.getSession(true)
	res, err := sess.DeleteFrom("${this.tableName}").Where("${where}").Exec()
	if err != nil {
		dbLogger.Errorf("DELETE %s WHERE %s error:%v", ${this.tableName}, ${where}, err)
		return 0, err
	}
	rows, err := res.RowsAffected()
	if err != nil {
		Signal("${this.tableName}", "delete", "${where}", nil, rows)
	}
	return rows, err
}
`
        this.writeStrings(code)
    }
	
	generateDataObjectFile() {
		this.output_file = fs.openSync(this.output_filename, 'w')
		this.writeHeader()
		this.writeStruct()
		this.genJsonMarshaler()
		this.genRowSource()
		if (this.hasId) {
			this.genGetByIds()
		}
		this.genGetByCPk()
		this.genGetAll()
		for (var fk of this.foreignKeys) {
			this.genGetByFk(fk.columnName)
		}
		for (var x of this.indexes) {
			this.genGetByIndex(x)
		}
		this.genInsert()
		if (this.hasId) {
			this.genUpdate()
		} else if (this.hasCompoundPrimaryKey) {
			this.genUpdateByCpk()
		}
		this.genUpdateWhere()
        if (this.hasCompoundPrimaryKey) {
            this.genDeleteByCpk()
        } else {
            this.genDelete()
        }
		fs.closeSync(this.output_file)
	}
}

var sf = new Loader.SchemaFiles(SCHEMA_DIR);
sf.traverse(function(schema_filename){
	var schema = new StructBuilder(SCHEMA_DIR, schema_filename, OUTPUT_DIR)
	schema.generateDataObjectFile()
});
