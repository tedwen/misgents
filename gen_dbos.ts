/// <reference path="typings/node/node.d.ts" />
import fs = require('fs');
import Loader = require('./SchemaLoader');

const PROJECT_NAME = process.argv[2] || '',
	  PARENT_DIR = __dirname.substring(0, __dirname.lastIndexOf('/')),
	  PROJECT_DIR = PROJECT_NAME ? PARENT_DIR + '/' + PROJECT_NAME : PARENT_DIR,
	  SCHEMA_DIR = PROJECT_DIR + '/schemas/',
	  OUTPUT_DIR = PROJECT_DIR + '/dbo/';

const HEADER_T = `package dbo

import (
	"log"
	"bytes"
`;

/**
 * StructBuilder
 */
class StructBuilder extends Loader.SchemaEntity {
	output_dir: string;
	output_filename: string;
	output_file: any;
	
	constructor(schema_dir, schema_filename, outdir) {
		super(schema_dir, schema_filename);
		this.output_dir = outdir;
		this.output_filename = `${outdir}g_${this.tableName}.go`;
	}
	
	writeStrings(...args: string[]) {
		var data: any = args.join('');
		fs.writeSync(this.output_file, data, null, null, null);
	}
	
	writeHeader() {
		var imps = [];
		for (var k in this.imports) {
			var v = this.imports[k];
			imps.push(`\t"${v}"`);
		}
		this.writeStrings(HEADER_T, imps.join('\n'), '\n)\n');
	}
	
	writeStruct() {
		var fields = this.properties;
		var lines = ['','type '+this.structName+' struct {'];
		for (var i=0; i<fields.length; i++){
			var fld = fields[i], 
				ss = ['\t', fld.capitalName, this.alignLeft(fld.capitalName), fld.dataType, this.alignLeft(fld.dataType), fld.jsonTag];
			lines.push(ss.join(''));
		}
		lines.push('}\n\n');
		this.writeStrings(lines.join('\n'));
	}
	
	genJsonMarshaler() {
		var lines = [];
		var fields = this.properties;
		for (var i=0; i<fields.length; i++){
			var fld = fields[i]
			if (fld.jsonTag == '`json:"-"`') continue
			if (fld.dataType == 'zero.String') {
				lines.push(`\tif m.${fld.capitalName}.Valid {`)
				if (i > 0) {lines.push('\t\tbuf.WriteString(",")')}
				lines.push('\t\tbuf.WriteString(`"'+fld.columnName+'":`)')
				lines.push('\t\tbuf.WriteString(`"`)')
				lines.push(`\t\tbuf.WriteString(m.${fld.capitalName}.String)`)
				lines.push('\t\tbuf.WriteString(`"`)')
				lines.push('\t}')
			} else {
				if (i > 0) {lines.push('\tbuf.WriteString(",")')}
				lines.push('\tbuf.WriteString(`"'+fld.columnName+'":`)')
				if (fld.dataType == 'int') {
					lines.push(`\tbuf.WriteString(strconv.Itoa(m.${fld.capitalName}))`)
				} else  {
					lines.push('\tbuf.WriteString(`"`)')
					if (fld.dataType == 'time.Time') {
						lines.push(`\tbuf.WriteString(m.${fld.capitalName}.Format("2006-01-02T15:04:05.000Z"))`)
					} else {
						lines.push(`\tbuf.WriteString(m.${fld.capitalName})`)
					}
					lines.push('\tbuf.WriteString(`"`)')
				}
			}
		}
		var code = `
func (m ${this.structName}) MarshalJSON() ([]byte, error) {
	var buf bytes.Buffer
	buf.WriteString("{")
${lines.join('\n')}
	buf.WriteString("}")
	return buf.Bytes(), nil
}
`;
		this.writeStrings(code)
	}
	
	genRowSource() {
		var fldmaps = this.makeScanParams();
		var name = this.structName;
		var lines = `
func ${name}Source() (interface{}, []interface{}) {
	e := ${name}{}
	return &e, []interface{}{${fldmaps}}
}
`;
		this.writeStrings(lines);
	}
	
	genGetByIds() {
		var name = this.structName;
		var tname = this.tableName;
		var lines = `
func (db *Database) Get${name}ByIds(ids string) (interface{}, error) {
	e, err := db.DoQueryByIds(${name}Source, "${tname}", ids)
	if err != nil {
		log.Println(err)
	}
	return e, err
}

func (db *Database) Get${name}ById(id int) (interface{}, error) {
	e, err := db.DoQueryById(${name}Source, "${tname}", id)
	if err != nil {
		log.Println(err)
	}
	return e, err
}
`;
		this.writeStrings(lines);
	}
	
	genGetByCPk() {
		if (!this.hasCompoundPrimaryKey) return;
		var name = this.structName;
		var tname = this.tableName;
		var fknames = [], fktypes = [], ps = [], whs = [];
		for (var fk of this.compoundPrimaryKeys) {
			fknames.push(fk.columnName);
			fktypes.push(fk.dataType);
			ps.push(fk.columnName+' '+fk.dataType);
			whs.push(fk.columnName+'=?');
		}
		var pks = ps.join(','), where = whs.join(' AND '), 
		qss = fknames.join(',');
		var lines = `
func (db *Database) Get${name}ByCPk(${pks}) (interface{}, error) {
	query := "SELECT * FROM ${tname} WHERE ${where}"
	es, err := db.DoQuery(${name}Source, query, ${qss})
	if err != nil {
		log.Println(err)
	}
	if len(es) > 0 {
		return es[0], nil
	}
	return nil, err
}
`;
		this.writeStrings(lines);
	}
	
	genGetAll(){
		var name = this.structName;
		var tname = this.tableName;
		var cname = this.pluralName;
		var lines = `
func (db *Database) GetAll${cname}(where string) ([]interface{}, error) {
	query := "SELECT * FROM ${tname}" + where
	es, err := db.DoQuery(${name}Source, query)
	if err != nil {
		log.Println(err);
	}
	return es, err
}
`;
		this.writeStrings(lines);
	}
	
	genGetByFk(fk: string) {
		var name= this.structName;
		var tname = this.tableName;
		var fkname = this.capitalise(fk.replace('_id','Id'));
		var fname = name + 'By' + fkname;
		var lines = `
func (db *Database) Get${fname}(id int) ([]interface{}, error) {
	query := "SELECT * FROM ${tname} WHERE ${fk}=?"
	es, err := db.DoQuery(${name}Source, query, id)
	if err != nil {
		log.Println(err)
	}
	return es, err
}

func (db *Database) Get${fname}s(ids string) ([]interface{}, error) {
	id, err := strconv.Atoi(ids)
	if err != nil {
		return nil, err
	}
	return db.Get${fname}(id)
}
`;
		this.writeStrings(lines);
	}
	
	genGetByIndex(x) {
		if (x.dataType != "string" && x.dataType != "zero.String") {
			console.log('dataType ',x.dataType, ' not implemented yet');
			return;
		}
		var code = `
func (db *Database) Get${this.structName}By${x.capitalName}(s string) ([]interface{}, error) {
	query := "SELECT * FROM ${this.tableName} WHERE ${x.columnName}=?"
	es, err := db.DoQuery(${this.structName}Source, query, s)
	if err != nil {
		log.Println(err)
	}
	return es, err
}
`;
		this.writeStrings(code);
	}
	
	genInsert() {
		var name = this.structName, tname = this.tableName;
		var fldmaps = this.makeFieldsMap();
		var lines = `
func (db *Database) PostNew${name}(body map[string]interface{}) (int, error) {
	fields := map[string]int{${fldmaps}}
    PrePost("${this.structName}", body)
	query, params, err := db.BuildInsertQuery("${tname}", body, fields)
	if err != nil {
		return 0, err
	}
	lastid, err := db.DoInsert(query, params...)
	if err != nil {
		log.Printf("Insert.error=%v for %v\\n", err, query)
		return 0, err
	}
	if lastid <= 0 {
		log.Printf("Insert.lastid=%v for %v\\n", lastid, query)
	}
    PostPost("${this.structName}", lastid, body)
	return lastid, nil
}
`;
		this.writeStrings(lines);
	}
	
	genUpdateWhere() {
		var fldmaps = this.makeFieldsMap(true)
		var code = `
func (db *Database) Put${this.structName}ByWhere(where string, body map[string]interface{}) (int, error) {
	fields := map[string]int{${fldmaps}}
	query, params, err := db.BuildUpdateQueryWhere("${this.tableName}", where, body, fields)
	if err != nil {
		return 0, err
	}
	rs, err := db.DoExec(query, params...)
	if err != nil {
		log.Printf("Update.erro=%v for %v\\n", err, query)
	}
	if rs <=0 {
		log.Printf("Update.rowsAffected=%v for %v\\n", rs, query)
	}
	return rs, err
}
`
		this.writeStrings(code)
	}
	
	genUpdate() {
		var fldmaps = this.makeFieldsMap(true);
		var code = `
func (db *Database) Put${this.structName}ById(id int, body map[string]interface{}) (int, error) {
	fields := map[string]int{${fldmaps}}
    PrePut("${this.structName}", id, body)
	query, params, err := db.BuildUpdateQuery("${this.tableName}", id, body, fields)
	if err != nil {
		return 0, err
	}
	rs, err := db.DoExec(query, params...)
	if err != nil {
		log.Printf("Update.erro=%v for %v\\n", err, query)
	}
	if rs <=0 {
		log.Printf("Update.rowsAffected=%v for %v\\n", rs, query)
	}
    PostPut("${this.structName}", id, rs, body)
	return rs, err
}

func (db *Database) Put${this.structName}ByIds(ids string, body map[string]interface{}) (int, error) {
	id, err := strconv.Atoi(ids)
	if err != nil {
		return 0, err
	}
	return db.Put${this.structName}ById(id, body)
}
`;
		this.writeStrings(code);
	}
	
	genUpdateByCpk() {
		var fldmaps = this.makeFieldsMap()
		var code = `
func (db *Database) Put${this.structName}ByCpk(pks, body map[string]interface{}) (int, error) {
	fields := map[string]int{${fldmaps}}
	query, params, err := db.BuildUpdateQueryPlus("${this.tableName}", pks, body, fields)
	if err != nil {
		return 0, err
	}
	rs, err := db.DoExec(query, params...)
	if err != nil {
		log.Printf("Update.erro=%v for %v\\n", err, query)
	}
	if rs <= 0 {
		log.Printf("Update.rowsAffected=%v for %v\\n", rs, query)
	}
	return rs, err
}
`
		this.writeStrings(code)
	}
	
	genDelete() {
		var code = `
func (db *Database) Delete${this.structName}ById(id int) (int, error) {
	query := "DELETE FROM ${this.tableName} WHERE id=?"
	rs, err := db.DoExec(query, id)
	if err != nil {
		log.Printf("Delete.erro=%v for %v\\n", err, query)
	}
	if rs <= 0 {
		log.Printf("Delete.rowsAffected=%v for %v\\n", rs, query)
	}
	return rs, err
}

func (db *Database) Delete${this.structName}ByIds(ids string) (int, error) {
	id, err := strconv.Atoi(ids)
	if err != nil {
		return 0, err
	}
	return db.Delete${this.structName}ById(id)
}
`;
		this.writeStrings(code);
	}
    
    genDeleteByCpk() {
		var fknames = [], fktypes = [], ps = [], whs = [];
		for (var fk of this.compoundPrimaryKeys) {
			fknames.push(fk.columnName);
			fktypes.push(fk.dataType);
			ps.push(fk.columnName+' '+fk.dataType);
			whs.push(fk.columnName+'=?');
		}
		var pks = ps.join(','), where = whs.join(' AND '), 
		qss = fknames.join(',');
        var code = `
func (db *Database) Delete${this.structName}ByCpk(${pks}) (int, error) {
    query := "DELETE FROM ${this.tableName} WHERE ${where}"
    rs, err := db.DoExec(query, ${qss})
    if err != nil {
        log.Printf("Delete.error=%v for %v",err,query)
    }
    if rs <= 0 {
        log.Printf("Delete.rowsAffected=%v for %v",rs,query)
    }
    return rs, err
}
`
        this.writeStrings(code)
    }
	
	generateDataObjectFile() {
		this.output_file = fs.openSync(this.output_filename, 'w')
		this.writeHeader()
		this.writeStruct()
		this.genJsonMarshaler()
		this.genRowSource()
		if (this.hasId) {
			this.genGetByIds()
		}
		this.genGetByCPk()
		this.genGetAll()
		for (var fk of this.foreignKeys) {
			this.genGetByFk(fk.columnName)
		}
		for (var x of this.indexes) {
			this.genGetByIndex(x)
		}
		this.genInsert()
		if (this.hasId) {
			this.genUpdate()
		} else if (this.hasCompoundPrimaryKey) {
			this.genUpdateByCpk()
		}
		this.genUpdateWhere()
        if (this.hasCompoundPrimaryKey) {
            this.genDeleteByCpk()
        } else {
            this.genDelete()
        }
		fs.closeSync(this.output_file)
	}
}

var sf = new Loader.SchemaFiles(SCHEMA_DIR);
sf.traverse(function(schema_filename){
	var schema = new StructBuilder(SCHEMA_DIR, schema_filename, OUTPUT_DIR)
	schema.generateDataObjectFile()
});
