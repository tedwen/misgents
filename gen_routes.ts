/* global process, __dirname */
/// <reference path="typings/node/node.d.ts" />
import fs = require('fs');
import Loader = require('./SchemaLoader');

const PROJECT_NAME = process.argv[2] || '',
	  PARENT_DIR = __dirname.substring(0, __dirname.lastIndexOf('/')),
	  PROJECT_DIR = PROJECT_NAME ? PARENT_DIR + '/' + PROJECT_NAME : PARENT_DIR,
	  SCHEMA_DIR = PROJECT_DIR + '/schemas/',
	  OUTPUT = PROJECT_DIR + '/api/';
// const SCHEMA_DIR = __dirname + '/../schemas/';
// const OUTPUT = __dirname + '/../api/';

var HANDLERS_1 = `package api

import (
	"log"
	"fmt"
    "strconv"
	"github.com/makeitsocial/${PROJECT_NAME}/ctx"
	"github.com/makeitsocial/${PROJECT_NAME}/dbo"
)

func PingDb(c *ctx.MisContext) {
	db := dbo.FindDatabaseByTable(c.GetShardStore(), "apps", false)
	if db.Ping() {
		c.JSON(200, ctx.MAP{"status": "OK"})
	} else {
		c.JSON(200, ctx.MAP{"status": "FAIL", "message": "Database connection error"})
	}
}
`;

var ROUTER_1 = `package api

import (
	"github.com/gin-gonic/gin"
	"github.com/makeitsocial/${PROJECT_NAME}/ctx"
)

func SetDataRoutes(rg *gin.RouterGroup) {
	var db = rg.Group("/db")
	{
		db.GET("", ctx.WrapCall(PingDb))
		db.GET("/ping", ctx.WrapCall(PingDb))
	}
`;


/**
 * FilesWriter for creating, saving g_router.go and go_handlers.go in /api
 * ROUTER_1 and HANDLERS_1 are saved at the beginning of these files.
 */
class FilesWriter {
	output_dir: string;
	routerFilename: string;
	router_file: any;
	handlersFilename: string;
	handlers_file: any;
	
	constructor(outdir) {
		this.output_dir = outdir
		this.createFiles()
	}
	
	writeRouter(...args: string[]) {
		var data: any = args.join('');
		fs.writeSync(this.router_file, data, null, null, null);
	}
	
	writeHandlers(...args: string[]) {
		var data: any = args.join('');
		fs.writeSync(this.handlers_file, data, null, null, null);
	}
	
	createFiles() {
		this.routerFilename = this.output_dir + 'g_router.go';
		this.router_file = fs.openSync(this.routerFilename, 'w');
		this.handlersFilename = this.output_dir + 'g_handlers.go';
		this.handlers_file = fs.openSync(this.handlersFilename, 'w');
		this.writeRouter(ROUTER_1)
		this.writeHandlers(HANDLERS_1)
	}

	closeRouter() {
		var data: any = "\n}\n"
		fs.writeSync(this.router_file, data, null, null, null)
		fs.closeSync(this.router_file)
	}

	closeFiles() {
		this.closeRouter()
		fs.closeSync(this.handlers_file)
	}
}
/*

	genGetByCPk() {
		if (!this.hasCompoundPrimaryKey) return;
		var name = this.structName;
		var tname = this.tableName;
		var fknames = [], fktypes = [], ps = [], whs = [];
		for (var fk of this.compoundPrimaryKeys) {
			fknames.push(fk.columnName);
			fktypes.push(fk.dataType);
			ps.push(fk.columnName+' '+fk.dataType);
			whs.push(fk.columnName+'=?');
		}
		var pks = ps.join(','), where = whs.join(' AND '), 
		qss = fknames.join(',');
		var lines = `
func (db *Database) Get${name}ByCPk(${pks}) (interface{}, error) {
	query := "SELECT * FROM ${tname} WHERE ${where}"
	es, err := db.DoQuery(${name}Source, query, ${qss})
	if err != nil {
		log.Println(err)
	}
	if len(es) > 0 {
		return es[0], nil
	}
	return nil, err
}
`;
		this.writeStrings(lines);
	}
*/

/**
 * Generator
 */
class Generator extends Loader.SchemaEntity {
	writers: FilesWriter;
	
	constructor(schema_dir, schema_filename, writers) {
		super(schema_dir, schema_filename);
		console.log('schema_dir=',schema_dir,'schema_filename=',schema_filename)
		this.writers = writers;
	}
	
	getCompoundKeys0(want_type=false) {
		var fknames = []
		for (var fk of this.compoundPrimaryKeys) {
			if (want_type) {
				fknames.push(fk)
			} else {
				fknames.push(fk.columnName)
			}
		}
		return fknames
	}
    getEntityNameFromId(id) {
        return id.replace('_id','s');   //assume adding s for plural
    }
    getCompoundKeyNames(want_type=false) {
        var fknames = []
        // skip first which is either owner_id, user_id by JWT or app_id by Auth
        for (var i=1; i<this.compoundPrimaryKeys.length; i++){
            var fk = this.compoundPrimaryKeys[i];
            if (i > 1) {
                var ename = this.getEntityNameFromId(fk.columnName)
                fknames.push(ename)
            }
            fknames.push(want_type ? fk : ':'+fk.columnName)
        }
        return fknames
    }
	
    // update: owner_id or first user_id will be implied in JWT, so not necessary in the path
	genCompoundKeyRoute(routes, rname) {
		var fknames = this.getCompoundKeyNames()
		var s = `r_${rname}.GET("/${fknames.join('/')}", ctx.WrapCall(Get${this.structName}Cpk))`
		routes.push(s)
		//r_tokens.GET("/:user_id/:app_id", ctx.WrapCall(GetTokenCpk))
	}
	
	genRoutes() {
		if (!this.pluralName) console.log('!!! pluralName null')
		var rname = this.pluralName.toLocaleLowerCase();
		var routes = [`r_${rname}.GET("", ctx.WrapCall(Get${this.pluralName}))`];
		if (this.hasId) {
			routes.push(`r_${rname}.GET("/:id", ctx.WrapCall(Get${this.structName}))`);
		} else if (this.hasCompoundPrimaryKey) {
			this.genCompoundKeyRoute(routes, rname)
		}
		routes.push(`r_${rname}.POST("", ctx.WrapCall(Post${this.structName}))`);
        if (this.hasId) {
    		routes.push(`r_${rname}.PUT("/:id", ctx.WrapCall(Put${this.structName}))`);
            routes.push(`r_${rname}.DELETE("/:id", ctx.WrapCall(Delete${this.structName}))`);
        } else if (this.hasCompoundPrimaryKey) {
            var fknames = this.getCompoundKeyNames(), fkns = fknames.join('/');
      		routes.push(`r_${rname}.PUT("/${fkns}", ctx.WrapCall(Put${this.structName}))`);
            routes.push(`r_${rname}.DELETE("/${fkns}", ctx.WrapCall(Delete${this.structName}))`)
        }
		
		return `
	var r_${rname} = rg.Group("/${rname}")
	{
		${routes.join('\n\t\t')}
	}
`;
	}
	
	generateRouter() {
		this.writers.writeRouter(this.genRoutes())
	}
	
	genHandlerGetOne() {
		return `
func Get${this.structName}(c *ctx.MisContext) {
	ids := c.Param("id")
    var db *dbo.Database
    if c.HasShardKeyValue() {
        db = dbo.FindDatabaseByIdx(c.GetShardStore(), "${this.tableName}", c.GetShardKeyValue(), true)
    } else {
        db = dbo.FindDatabaseByIds(c.GetShardStore(), "${this.tableName}", ids, true)
    }
	e, err := db.Get${this.structName}ByIds(ids)
	if err != nil {
		c.JSON(500, ctx.MAP{"error": err.Error()})
	} else if e == nil {
		c.JSON(404, ctx.MAP{"error": "Not found"})
	} else if c.NeedEncrypt() {
		ctx.EncryptData(c, e, c.GetEncryptKey())
	} else {
		c.JSON(200, e)
	}
}
`;
	}
	
	genHandlerGetCpk() {
		var fks = this.compoundPrimaryKeys, nms = [], vs = [], fkname = '';
		for (var fk of fks) {
			var kn = fk.columnName
			var ts = fk.dataType == 'int' ? '%s' : "'%s'"
			nms.push(kn+'='+ts)
            if (ts == '%s' && fk.isForeignKey) {
                fkname = kn
            }
            if (kn == 'app_id') {
                vs.push('c.GetAppIds()')
            } else if (kn == 'owner_id' || kn == 'user_id') {
                vs.push('c.GetUserIds()')
            } else {
                vs.push(`c.Param("${fk.columnName}")`)
            }
		}
		return `
func Get${this.structName}Cpk(c *ctx.MisContext) {
	//sql := fmt.Sprintf(" WHERE owner_id=%s AND name='%s' AND member_id=%s", c.Param("owner_id"), c.Param("name"), c.Param("member_id"))
	sql := fmt.Sprintf(" WHERE ${nms.join(' AND ')}", ${vs.join(', ')})
	db := dbo.FindDatabaseByTablex(c.GetShardStore(), "${this.tableName}", ${vs[0]}, true)
    if db == nil {
        c.AbortWithErrMsg(500, "Database not found")
        return
    }
	es, err := db.GetAll${this.pluralName}(sql)
	if err != nil {
		c.JSON(500, ctx.MAP{"error": err.Error()})
		return
	}
	if len(es) < 1 {
		c.JSON(404, ctx.MAP{"error": "Not Found"})
		return
	}
	e := es[0]
	if c.NeedEncrypt() {
		ctx.EncryptData(c, e, c.GetEncryptKey())
	} else {
		c.JSON(200, e)
	}
}
`
	}
	
	genHandlerGetAll() {
        var sql = '""'
        var findcont = `(c.GetShardStore(), "${this.tableName}", true)`
        if (this.hasCompoundPrimaryKey) {
            var cs = [], vs = [], fks = this.compoundPrimaryKeys
            var shkey = this.getGetValueByField(fks[0].columnName,'')
            findcont = `x(c.GetShardStore(), "${this.tableName}", ${shkey}, true)`
            for (var i=0; i<fks.length-1; i++) {
                var fk = fks[i]
                var kn = fk.columnName
                var ts = fk.dataType=='int'?'Int':''
                var ks = ts=='Int'?'%s':"'%s'"
                var gv = this.getGetValueByField(kn, '')
                cs.push(`${kn}=${ks}`)
                vs.push(gv)
            }
            var sqlcs = cs.join(' AND ')
            sql = `fmt.Sprintf(" WHERE ${sqlcs}",${vs.join(',')})`
        }
		return `
func Get${this.pluralName}(c *ctx.MisContext) {
    var db *dbo.Database
    if c.HasShardKeyValue() {
        db = dbo.FindDatabaseByTablex(c.GetShardStore(), "${this.tableName}", c.GetShardKeyValue(), true)
    } else {
        db = dbo.FindDatabaseByTable${findcont}
    }
    if db == nil {
        c.AbortWithErrMsg(500, "Database not found")
        return
    }
	sql := ${sql}
	if c.HasSqlWhere() {
		sql = c.GetSqlWhere()
	}
	es, err := db.GetAll${this.pluralName}(sql)
	if err != nil {
		c.JSON(500, ctx.MAP{"error": err.Error()})
	} else if c.NeedEncrypt() {
		ctx.EncryptData(c, es, c.GetEncryptKey())
	} else {
		c.JSON(200, es)
	}
}
`;
	}
	
	/**
	 * POST create new entity
	 */
	genHandlerPostNew() {
        var prefill = ''
        if (this.hasCompoundPrimaryKey) {
            var fks = this.compoundPrimaryKeys, cs = []
            for (var i=0; i<fks.length-1; i++){
                var fk = fks[i], kn = fk.columnName, ts = fk.dataType=='int'?'Int':''
                var gv = this.getGetValueByField(kn, ts)
                cs.push(`data["${kn}"] = ${gv}`)
            }
            prefill = `\n    ${cs.join('\n    ')}`
        }
		var code = `
func Post${this.structName}(c *ctx.MisContext) {
    data, err := c.GetBodyJsonMap()
	if err != nil {
		log.Printf("JSONize error:%v", err)
		c.JSON(500, ctx.MAP{"error": "Invalid data"})
		return
	}
	if c.HasSqlPreset() {
		for p, v := range c.GetSqlPreset() {
			data[p] = v
		}
	}
	db,pk := dbo.FindDatabaseForInsert(c.GetShardStore(), "${this.tableName}", data)
    if db == nil {
        c.AbortWithErrMsg(500, "Database not found")
        return
    }
	if pk > 0 {
		data["id"] = pk
	}${prefill}
	r, err := db.PostNew${this.structName}(data)
	if err != nil {
		log.Printf("Post${this.structName} error:%v\\n", err)
		c.JSON(500, ctx.MAP{"error": err.Error()})
	} else {${this.getDelPassword()}
        ctx.SendData("${this.tableName}", "new", data)
		c.JSON(201, ctx.MAP{"id": r, "status": "OK"})
	}
}
`;
        let appr = `data["id"] = r
        c.JSON(201, data)`
        if (this.structName=='App') {
            return code.replace('c.JSON(201, ctx.MAP{"id": r, "status": "OK"})',appr)
        } else if (this.hasCompoundPrimaryKey) {
            return code.replace('"id": r, ', '').replace('r, err :','_, err ')
        }
        return code
	}
	
    getDelPassword() {
        return this.tableName=='users' ? '\n        delete(data, "password")\n        delete(data, "password2")' : ''
    }
    
	genHandlerPut() {
		return `
func Put${this.structName}(c *ctx.MisContext) {
	ids := c.Param("id")
    data, err := c.GetBodyJsonMap()
	if err != nil {
		log.Printf("JSONize error:%v", err)
		c.JSON(500, ctx.MAP{"error": "Invalid data"})
		return
	}
	db := dbo.FindDatabaseForUpdate(c.GetShardStore(), "${this.tableName}", ids)
    if db == nil {
        c.AbortWithErrMsg(500, "Database not found")
        return
    }
	r := 0
	if c.HasSqlWhere() {
		r, err = db.Put${this.structName}ByWhere(c.GetSqlWhere(ids), data)
	} else {
		r, err = db.Put${this.structName}ByIds(ids, data)
	}
	if err != nil {
		log.Printf("Post${this.structName} error:%v\\n", err)
		c.JSON(500, ctx.MAP{"error": err.Error()})
	} else {${this.getDelPassword()}
        if id, er := strconv.Atoi(ids); er != nil {
            data["id"] = id
        }
        ctx.SendData("${this.tableName}", "put", data)
		c.JSON(200, ctx.MAP{"status": "OK", "rows": r})
	}
}
`;
	}
	
    getGetValueByField(cname, want_type='Int') {
        var s = want_type=='Int' ? '' : 's', gu = `c.GetUserId${s}()`
        var rv = {app_id: `c.GetAppId${s}()`, owner_id: gu, user_id: gu}[cname]
        return rv || `c.Param${want_type}("${cname}")`
    }
    
	genHandlerPutByCpk() {
		var fks = this.compoundPrimaryKeys, vs = [], dus = [],
            shkey = this.getGetValueByField(fks[0].columnName,'');
		for (var fk of fks) {
			var kn = fk.columnName
			var ts = fk.dataType == 'int' ? 'Int' : ""
            var gv = this.getGetValueByField(kn, ts)
			vs.push(`"${kn}":${gv}`)
            dus.push(`data["${kn}"] = ${gv}`)
		}
		var params = vs.join(','), dupdates = dus.join('\n        ')
		return `
func Put${this.structName}(c *ctx.MisContext) {
	pks := map[string]interface{}{${params}}
    data, err := c.GetBodyJsonMap()
	if err != nil {
		c.JSON(500, ctx.MAP{"error": "Invalid data"})
		return
	}
	shkey := ${shkey}
	db := dbo.FindDatabaseForUpdate(c.GetShardStore(), "${this.tableName}", shkey)
    if db == nil {
        c.AbortWithErrMsg(500, "Database not found")
        return
    }
	r := 0
	if c.HasSqlWhere() {
		r, err = db.Put${this.structName}ByWhere(c.GetSqlWhere(pks), data)
	} else {
		r, err = db.Put${this.structName}ByCpk(pks, data)
	}
	if err != nil {
		c.JSON(500, ctx.MAP{"error": err.Error()})
    } else if r == 0 {
        c.JSON(500, ctx.MAP{"error": "Nothing changed"})
	} else {${this.getDelPassword()}
        ${dupdates}
        ctx.SendData("${this.tableName}","put",data)
		c.JSON(200, ctx.MAP{"status": "OK", "rows": r})
	}
}
`
	}
    
    genHandleDeleteByCpk() {
        var fks = this.compoundPrimaryKeys, vs = [], ps = [], dus = [];
        var shkey = this.getGetValueByField(fks[0].columnName,'')
		for (var fk of fks) {
			var kn = fk.columnName
			var ts = fk.dataType == 'int' ? 'Int' : ""
			//vs.push(`"${kn}":c.Param${ts}("${kn}")`)
            vs.push(kn)
            var gv = this.getGetValueByField(kn, ts)
            ps.push(`${kn} := ${gv}`)
            dus.push(`"${kn}": ${kn}`)
            // if (fk.dataType == 'string') {
            //     ps.push(`${kn} := c.Param("${kn}")`)
            // } else {
            //     // int assumed
            //     ps.push(`${kn} := c.ParamInt("${kn}")`)
            // }
		}
        var param_assigns = ps.join('\n    ')  //ps := c.Query("number"); number,_ := strconv.Atoi(ps)
		var params = vs.join(',')
        var dupdates = dus.join(', ')
        var code = `
func Delete${this.structName}(c *ctx.MisContext) {
    shkey := ${shkey}
    db := dbo.FindDatabaseForUpdate(c.GetShardStore(), "${this.tableName}", shkey)
    if db == nil {
        c.AbortWithErrMsg(500, "Database not found")
        return
    }
    ${param_assigns}
    _, err := db.Delete${this.structName}ByCpk(${params})
    if err != nil {
        log.Printf("Delete${this.structName}:%v", err)
        c.JSON(500, ctx.MAP{"error": err.Error()})
    } else {
        vals := ctx.MAP{${dupdates}}
        ctx.SendData("${this.tableName}","del",vals)
        c.String(204, "")
    }
}
`
        return code
    }
	
	genHandlerDelete() {
		return `
func Delete${this.structName}(c *ctx.MisContext) {
	ids := c.Param("id")
    var db *dbo.Database
    if c.HasShardKeyValue() {
        db = dbo.FindDatabaseByIdx(c.GetShardStore(), "${this.tableName}", c.GetShardKeyValue(), false)
    } else {
        db = dbo.FindDatabaseByIds(c.GetShardStore(), "${this.tableName}", ids, false)
    }
    if db == nil {
        c.AbortWithErrMsg(500, "Database not found")
        return
    }
	_, err := db.Delete${this.structName}ByIds(ids)
	if err != nil {
		log.Printf("Post${this.structName} error:%v\\n", err)
		c.JSON(500, ctx.MAP{"error": err.Error()})
	} else {
        if id, er := strconv.Atoi(ids); er != nil {
            ctx.SendData("${this.tableName}", "del", ctx.MAP{"id":id})
        }
		c.String(204, "")
	}
}
`;
	}
	
	generateHandlers() {
		if (this.hasId) {
			this.writers.writeHandlers(this.genHandlerGetOne())
		} else if (this.hasCompoundPrimaryKey) {
			this.writers.writeHandlers(this.genHandlerGetCpk())
		}
		this.writers.writeHandlers(this.genHandlerGetAll())
		this.writers.writeHandlers(this.genHandlerPostNew())
		if (this.hasId) {
			this.writers.writeHandlers(this.genHandlerPut())
		} else if (this.hasCompoundPrimaryKey) {
			this.writers.writeHandlers(this.genHandlerPutByCpk())
		}
        if (this.hasCompoundPrimaryKey) {
            this.writers.writeHandlers(this.genHandleDeleteByCpk())
        } else {
            this.writers.writeHandlers(this.genHandlerDelete())
        }
	}
	
	generate() {
		this.generateRouter()
		this.generateHandlers()
	}
}

let joiners = `
        r_circles.GET("/:number/joiners", ctx.WrapCall(GetJoiners))
        r_circles.GET("/:number/joiners/:joiner_id", ctx.WrapCall(GetJoinerCpk))
        r_circles.POST("/:number/joiners", ctx.WrapCall(PostJoiner))
        r_circles.PUT("/:number/joiners/:joiner_id", ctx.WrapCall(PutJoiner))
        r_circles.DELETE("/:number/joiners/:joiner_id", ctx.WrapCall(DeleteJoiner))
    `

/**
 * ApiBuilder
 */
class ApiBuilder {
	writers: FilesWriter;
	
	constructor(outdir) {
		this.writers = new FilesWriter(outdir);
	}
	
	run() {
		new Loader.SchemaFiles(SCHEMA_DIR).traverse((schema_filename)=>{
			new Generator(SCHEMA_DIR, schema_filename, this.writers).generate();
		});
		this.writers.closeFiles();
        this.update_routes()
	}
    
    update_routes() {
        var fname = OUTPUT+'g_router.go'
        var txt = fs.readFileSync(fname,'utf8')
        var n1 = txt.indexOf('var r_joiners =')
        if (n1 < 0) {
            return
        }
        var n2 = txt.indexOf('var r_',n1+10)
        txt = txt.substring(0,n1)+txt.substr(n2)
        n1 = txt.indexOf('var r_circles ='), n2 = txt.indexOf('}',n1)
        txt = txt.substring(0, n2)+joiners+txt.substr(n2);
        fs.writeFileSync(fname, txt)
    }
}

new ApiBuilder(OUTPUT).run()
