# The MisAuth generators

To simplify the task of writing the routine codes such as data access objects that depend on schemas, we have created a suite of generators using TypeScript.

These scripts are being updated with the development of MisAuth, and saved as a standalone repo in order to be reused or referenced in other projects.

## Features
* Parse JSON schemas to generate SQL DDL for MySQL
* Generate DAO Go modules with basic accessor methods
* Generate basic endpoints handlers using Gin web framework
* Generate some mock data and test scripts
* Generate i18n message files

## Install TypeScript
We assume NodeJs/NPM is installed.
```sh
npm install -g typescript
```
## Install TypeScript definition manager
```sh
npm install tsd -g
```

## Download the generator repo
```sh
git clone git@bitbucket.org:tedwen/misgents.git
```

## Install Nodejs type defs
```sh
tsd install node
```

## Configure directories
At the moment, the directories for source and target files are hard-coded.
And this repo must be cloned under the working project base directory, where the following subdirectories are used:
* schemas/  this folder contains all the JSON schemas with one mapping to a database table
* rds/  this folder will be used to contain the generated *.sql files.
* dbo/  this folder will be used for the generated *.go files for data access.
* api/  this folder is for the generated endpoint handlers as router.go and handlers.go

## Local modification
Use Visual Studio Code to modify the ts files. And run "./autoc.sh" to compile TypeScript files to JavaScript ones on the fly.

## Run the generators
```sh
./gen_all.sh
```



# NEW updates
To generate more flexible API routes and handlers, modify the gen_routes.ts to support:
* generate cascaded and parameterised routes for foreign key constraints, for example, with allocations having:
```JSON
  "product_id": {
      "_fk": "products(`id`)"
  }
 ```
 will generate two equivalent routes:
 ```JSON
    /v2/products/{id}/allocations
    /v2/allocations?product_id={id}
```
* parse "_routes" for excluded endpoints and special ones:
```JSON
  "_routes": {
      "exclude": ["GET_ALL","product_id"],
      "/products/{pid}/datetime/{dt}/allocations": {
          "methods": ["GET"],
          "refer": "/allocations",
          "params": {
              "pid": "product_id",
              "dt": "datetime"
          }
      }
  }
```
* apply different authentication methods accordingly
    * apikey - this will just check for an X-ApiKey in the request header against Apps
    * jwt - this is required for all user-level updates like booking
    * manager - this requires the user with the jwt exists as the manager of the resource
    * super/admin - this requires that the user with the jwt exists as a superuser/admin role
if no auth specified, the default will be to have apikey for GET and manager for other methods.

* add generation of admin webpages like Django

* generate ER and UML class diagrams from json schema
