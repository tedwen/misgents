/// <reference path="typings/node/node.d.ts"/>
import fs = require('fs')

let rename_imports = () => {
    let replace_misauth_in_file = (fn) => {
        var txt = fs.readFileSync(fn, 'utf8')
        if (txt.indexOf('/misauth/') > 0) {
            fs.writeFileSync(fn, txt.replace(/\/misauth\//g,'/misgroups/'))
        }
    }
    let ren_import = (folder) => {
        for (var fn of fs.readdirSync(folder)) {
            if (fn.indexOf('.go')>0) {
                replace_misauth_in_file(folder+'/'+fn)
            }
        }
    }
    ren_import('../api')
    ren_import('../auth')
    ren_import('../ctx')
}

let capsingle = (s:string) => {
    var n = s.length-1,
        f = s.charAt(0).toUpperCase(),
        t = s.charAt(n)=='s' ? s.substring(0, n) : s, 
        r = t.substr(1);
    return f + r
}

let rename_shard_test=()=>{
    let handle_file = (fn) => {
        var txt = fs.readFileSync(fn,'utf8')
        if (txt.indexOf('"misauth')>0) {
            txt = txt.replace(/"misauth/g, '"misgroups')
            if (txt.indexOf('"misers')>0) {
                txt = txt.replace(/"misers/g, '"misgros')
            }
            fs.writeFileSync(fn, txt)
        }
    }
    handle_file('../.ssh/shards_test.json')
    handle_file('../.ssh/shards_live.json')
}

let update_api_routes = () => {
    var txt = fs.readFileSync('../api/g_router.go','utf8')
    var n1 = txt.indexOf('var r_'), n1 = txt.indexOf('}', n1)
    // console.log(txt, n)
    var re = /Group\("\/(\w+)"\)/g
    var gs = [], m
    while (m = re.exec(txt)) {
        gs.push(m[1])
    }
    var output = [txt.substring(0, n1)]
    for (var g of gs) {
        if (g == 'db' || g == 'groups' || g == 'users' || g == 'usergroups') continue
        var entity = capsingle(g)
        output.push('')
        output.push(`\t\tr_groups.GET("/:gid/${g}", ctx.WrapCall(Get${entity}s))`)
        output.push(`\t\tr_groups.GET("/:gid/${g}/:id", ctx.WrapCall(Get${entity}))`)
        output.push(`\t\tr_groups.POST("/:gid/${g}", ctx.WrapCall(Post${entity}))`)
        output.push(`\t\tr_groups.PUT("/:gid/${g}/:id", ctx.WrapCall(Put${entity}))`)
        output.push(`\t\tr_groups.DELETE("/:gid/${g}/:id", ctx.WrapCall(Delete${entity}))`)
    }
    output.push('\t}\n}\n')
    fs.writeFileSync('../api/g_router.go', output.join('\n'))
}

let add_where_in_dbo_deletes=()=>{
}

(()=>{
    //rename_imports()
    //rename_shard_test()
    //update_api_routes()
    add_where_in_dbo_deletes()
})()