/* global process, __dirname */
/// </// <reference path="typings/node/node.d.ts" />

import fs = require('fs');

const PROJECT_NAME = process.argv[2] || '',
	  PARENT_DIR = __dirname.substring(0, __dirname.lastIndexOf('/')),
	  PROJECT_DIR = PROJECT_NAME ? PARENT_DIR + '/' + PROJECT_NAME : PARENT_DIR,
	  SCHEMA_DIR = PROJECT_DIR + '/schemas/',
	  OUTPUT = PROJECT_DIR + '/rds/';

/**
 * Get id _mysql type from related schema file.
 * @param _fk: like 'apps(`id`)','clients(`number`)'
 */
function find_fk_type(_fk) {
	var ss = _fk.match(/\w+/g), //s = _fk.split('(')[0],
		s = ss[0], c = ss[1], 
		sfilename = s.charAt(0).toUpperCase() + s.substr(1);
	var d = JSON.parse(fs.readFileSync(SCHEMA_DIR + sfilename + '.json', 'utf8'));
	return d.properties[c]._mysql || 'INT';
}

/**
 * Add back single quotes around table and field names if required.
 * @param _fk: like "apps(id)" or "apps(`id`)"
 * @return `apps`(`id`)
 */
function mysql_quote(_fk) {
	var ss = _fk.split(/[(,)]/), t = null, ss2 = [];
	for (var i=0; i<ss.length; i++) {
		var s = ss[i].trim();
		if (s && s!=''){
			var ts = (s.charAt(0)=='`') ? s : '`'+s+'`';
			if (t == null)
				t = ts;
			else
				ss2.push(ts);
		}
	}
	return t + '(' + ss2.join(',') + ')';   
}

// gen_dll_from_json_schema will add table names with fks if any
var tables_with_fks = []; //[{name:'users',fks:[]},..]
// this function reorders the table names in dependency order, ie, left-most is the independent.
function order_by_dependency(){
	var ordered = [];
	for (var i=0; i<tables_with_fks.length; i++) {
		var t = tables_with_fks[i], tx = ordered.indexOf(t.name);
		//token.fk=user_id, app_id, users and apps must appear before token
		var x = (tx < 0) ? ordered.length : tx;
		for (var j=0;j<t.fks.length;j++){
			var fk = t.fks[j], fx = ordered.indexOf(fk);
			if (fx < 0){
				ordered[x] = fk; x++;
			} else if (fx > x){
				var o = ordered.splice(fx, 1);
				ordered.splice(x, 0, o[0]);
			}
		}
		if (tx < 0)	{
			if (t.fks.length == 0) {
				ordered.splice(0, 0, t.name);
			} else {
				ordered[x] = t.name;
			}
		} else if (tx > 0 && t.fks.length == 0) {
			var b = ordered.splice(tx, 1);
			ordered.splice(0, 0, b[0]);
		}
		console.log('----i=',i,'t=',t,'tx=',tx,'x=',x,'ordered:',ordered);
	}
	// console.log('ordered:',ordered);
	return ordered;
}

// generate g_loader.sql and g_dropper.sql
function gen_loader_dropper(output_dir) {
	var loader_filename = output_dir + 'g_loader.sql',
		dropper_filename = output_dir + 'g_dropper.sql',
		tables = order_by_dependency(),
		asc = [], desc = [];
	for (var i=0;i<tables.length; i++){
		var tn = tables[i];
		asc.push('source g_'+tn+'.sql;');
		desc.splice(0,0,'drop table if exists '+tn+';');
	}
	fs.writeFileSync(loader_filename, asc.join('\n'));
	fs.writeFileSync(dropper_filename, desc.join('\n'));
}

/**
 * Generate SQL DDL from JSON schema file.
 * @param schema_filename: like 'Users.json'
 * @param output_dir: '../rds'
 * 
 * JSON schema names are used for table names.
 * The schemas are all flat, with all fields in the root.properties.
 * Those in the required array will have NOT NULL added.
 * Fields named "id" will be the primary key with auth_crement.
 * Fields of type "integer" will be translated into INT (4-byte) type and "int" will be INT (4-byte) type.
 * Number fields will be "DOUBLE" unless specified.
 * String fields with a maxLength will be translated into a VARCHAR(maxLength), and
 * those without maxLength will be a TEXT (Clob)
 * String fields with format="date-time" will be DATETIME (8-byte, the 4-byte TIMESTAMP will run out in 2038)
 * If string format="uri" or "email", use VARCHAR(255)
 * Output filenames all prefixed with '_' for easier removal.
 */
function gen_ddl_from_json_schema(schema_filename, output_dir, cb) {
	var fname = SCHEMA_DIR + schema_filename;
	try {
		var obj = JSON.parse(fs.readFileSync(fname, 'utf8'));
	} catch (e){
		console.log(schema_filename, ':', e);
		return cb('File not found');
	}
	if (!obj['$schema']) {
		console.log(schema_filename, ' is not JSON schema');
		return cb('Not JSON schema');
	}
	var fields = [], pk = null, fks = [], uniques = [], idxs = [], idxkeys = [], _fks = [];
	for (var k in obj.properties) {
		var v = obj.properties[k], df = '',
			ftp = v.maxLength ? 'VARCHAR('+v.maxLength+')' : 'TEXT',
			nnul = (obj.required || []).indexOf(k)>=0 || k=='id' ? ' NOT NULL' : '';
		// 	nnul = ' NOT NULL',
		// 	df = v.type=='string' ? " DEFAULT ''" : ' DEFAULT 0';
		// if (v.format == 'date-time') df = ''; //mysql 
		
		// determine field data type
		if (v.type == 'string'){
			if (v.format) {
				if (v.format=='email' || v.format=='uri') {
					ftp = 'VARCHAR(255)';
				} else if (v.format=='date-time') {
					ftp = 'DATETIME';
				}
			}
			if (v._mysql) ftp = v._mysql; //override the default
		}
		else if (v.type == 'integer'){
			if (k == 'id'){
				df = ' AUTO_INCREMENT';
			}
			ftp = v._mysql || 'INT';
			if (v._fk) {
				ftp = find_fk_type(v._fk);
			}
		}
		else if (v.type == 'number'){
			ftp = v._mysql || 'DOUBLE';
		}
	
		// add DEFAULT if any
		if (typeof(v.default)!='undefined') {
			var dfs = (typeof(v.default)=='string') ? "'"+v.default+"'" : v.default;
			df = ' DEFAULT ' + dfs;
		} else if (k == 'created_at') {
			df = ' DEFAULT CURRENT_TIMESTAMP';
		}
		// if retime on update: ' ON UPDATE CURRENT_TIMESTAMP' (mysql 5.6.5+)
		
		//unique ?
		if (v._unique) {
			uniques.push(k);
		}
		
		if (ftp.indexOf('NOT NULL')>=0) nnul = '';
		
		fields.push('  `' + k + '` ' + ftp + nnul + df);
		
		// add index if any
		if (v._index) {
			if (v._index == 'unique')
				idxkeys.push(k);
			else
				idxs.push(k);
		}
		
		// add primary key constraint
		if (!pk && k == 'id') pk = '  PRIMARY KEY (`' + k + '`)';
		if (v._pk) {
			if (!pk){
				pk = [k];
			} else {
				pk.push(k);
			}
		}
		
		// add foreign key constraint if any
		if (v._fk) {
			fks.push('  FOREIGN KEY (`' + k + '`) REFERENCES ' + mysql_quote(v._fk));
			var _s = v._fk.split('(')[0].replace('`','');
			_fks.push(_s);
		}
	} //end of for loop
	
	if (pk) {
		if (Array.isArray(pk)){
			fields.push('  PRIMARY KEY (`' + pk.join('`,`') + '`)');
		} else {
			fields.push(pk);
		}
	}
	if (fks && fks.length > 0) {
		fields.push(fks.join(',\n'));
	}
	if (idxkeys.length > 0) {
		fields.push('  UNIQUE INDEX (`' + idxkeys.join('`,`') + '`)');
	}
	if (idxs.length > 0) {
		fields.push('  INDEX (`' + idxs.join('`,`') + '`)');
	}
	if (uniques.length > 0) {
		fields.push('  UNIQUE (`' + uniques.join('`,`') + '`)');
	}
	
	var schema_name = schema_filename.replace('.json',''),
		tablename = schema_name.toLowerCase(),
		save_filename = output_dir + 'g_' + tablename + '.sql';
	var fst = 'CREATE TABLE IF NOT EXISTS `' + tablename + '` (\n',
		flds = fields.join(',\n');
		
	fs.writeFileSync(save_filename, fst + flds + '\n);\n');
	
	tables_with_fks.push({name: tablename, fks: _fks});
	
	cb();
}

function collect_schemas_to_gen(output_dir) {
	for (var fn of fs.readdirSync(SCHEMA_DIR)) {
		if (/^[A-Z][a-zA-Z]+s\.json$/.test(fn)) {
			gen_ddl_from_json_schema(fn, output_dir, ()=>{
				//console.log(fn, 'done');
			});
		}
	}
	gen_loader_dropper(output_dir);
	console.log('All done!');
}

collect_schemas_to_gen(OUTPUT);
