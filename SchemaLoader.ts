/// <reference path="typings/node/node.d.ts" />
import fs = require('fs');

/**
 * SchemaProperty for dbo struct building
 */
export class SchemaProperty {
	columnName: string;
	capitalName: string;
	dataType: string;
	jsonTag: string;
	isId: boolean;
	isPrimaryKey: boolean; //if _pk, instead of id
	isForeignKey: boolean;
	isIndexed: boolean;
	foreignKey: string;
	editable: boolean;
	generator: string;
    path: string; //middle path like circles/:number
	hasDefault: boolean;
	
	constructor(propName, propObject) {
		this.columnName = propName;
		this.capitalName = this.mergeCapitalise(propName);
		this.jsonTag = '`json:"' + propName + ',omitempty"`';
		if (propObject.format == 'password') {
			this.jsonTag = '`json:"-"`';
		}
		this.convertDataType(propName, propObject);
		this.isId = propName == 'id';
		this.isPrimaryKey = propObject._pk ? true : false;
		this.isForeignKey = propObject._fk ? true : false;
		this.isIndexed = propObject._index ? true : false;
		if (this.isForeignKey) this.foreignKey = propObject._fk;
		this.editable = typeof(propObject._editable)=='undefined' ? true : propObject._editable==true;
		if (this.editable && (this.isId || this.isPrimaryKey || propName=='created_at')) this.editable = false;
        this.path = propObject._path || '';
		this.generator = propObject._gen || null;
		this.hasDefault = typeof(propObject.default)=='undefined' ? false : true; 
	}
	
	convertDataType(name, prop) { //for Golang
		this.dataType = {integer:'int', string:'string'}[prop.type];
		if (!this.dataType) {
			console.log(`!!! type "${prop.type}" for "${name}" in JSON schema`);
			return;
		}
		if (prop.format == 'date-time') {
			this.dataType = 'time.Time';
		}
	}
	
	/**
	 * client_id => ClientId
	 */
	mergeCapitalise(s) {
		var s2 = s.split('_');
		for (var i in s2) {
			s2[i] = this.capitalise(s2[i]);
		}
		return s2.join('');
	}
	
	capitalise(s) {
		if (!s || s.length < 1) return s;
		return s.charAt(0).toUpperCase() + s.substr(1);
	}

	foreignKeyTable() {
		if (this.isForeignKey) {
			let fk = this.foreignKey, n = fk.indexOf('(');
			return fk.substring(0, n);
		}
		return null;
	}
}

/**
 * SchemaEntity
 */
export class SchemaEntity {
	available: boolean;
	singularName: string;
	pluralName: string;
	structName: string;
	tableName: string;
	schema: Object;
	properties: SchemaProperty[] = [];
	hasId: boolean;
	hasCompoundPrimaryKey: boolean;
	compoundPrimaryKeys: SchemaProperty[] = [];
	imports: {} = {};
	foreignKeys: SchemaProperty[] = [];
	indexes: SchemaProperty[] = [];
	routes: {} = null;
	
	constructor(schema_dir,schema_filename) {
		if (!this.loadJsonSchema(schema_dir, schema_filename)) return;
		var name = schema_filename.replace('.json', '');
		this.pluralName = name; //like Users
		this.tableName = name.toLowerCase(); //users
		this.singularName = this.singularise(name); //User
		this.structName = this.singularName; //User
		//
		this.parseProperties();
		//if (this.foreignKeys.length > 0) {
			this.imports['strconv'] = 'strconv';
		//}
		this.routes = this.schema['_routes'];
	}
	
	loadJsonSchema(sdir, sfilename) {
		var fname = sdir + (/\/$/.test(sdir)?'':'/') + sfilename;
		try {
			this.schema = JSON.parse(fs.readFileSync(fname, 'utf8'));
			this.available = true;
		} catch (e) {
			console.log(fname, ':', e);
			this.available = false;
		}
		return this.available;
	}
	
	parseProperties() {
		var required = this.schema['required'] || [];
		for (var k in this.schema['properties']) {
			var v = this.schema['properties'][k];
			var sp = new SchemaProperty(k, v)
			this.properties.push(sp);
			if (sp.isId) this.hasId = true;
			if (sp.isPrimaryKey) {
				this.hasCompoundPrimaryKey = true;
				this.compoundPrimaryKeys.push(sp);
			}
			if (sp.isForeignKey) {
				//this.foreignKeys.push(sp.columnName);
				this.foreignKeys.push(sp)
			}
			if (sp.isIndexed) {
				this.indexes.push(sp);
			}
			/*if (sp.dataType == 'string' && required.indexOf(sp.columnName) < 0) {
				sp.dataType = 'zero.String'; //NULLable fields for Golang!
				this.imports['zero'] = 'github.com/guregu/null/zero';
			} else if (sp.dataType == 'time.Time'){
				this.imports['time'] = 'time';
			}*/
			if (['id','created_at'].indexOf(sp.columnName) < 0 && required.indexOf(sp.columnName) < 0 && !sp.hasDefault) {
				switch (sp.dataType) {
					case 'string':
						sp.dataType = 'zero.String';
						break;
					case 'time.Time':
						sp.dataType = 'zero.Time';
						break;
					case 'int':
						sp.dataType = 'zero.Int';
						break;
				}
			}
			if (sp.dataType == 'time.Time') this.imports['time'] = 'time';
			if (sp.dataType.indexOf('zero.') == 0) this.imports['zero'] = 'github.com/guregu/null/zero';
		}
	}
	
	capitalise(s) {
		if (!s || s.length < 1) return s;
		return s.charAt(0).toUpperCase() + s.substr(1);
	}

	capitalise2(s) {
		if (!s || s.length <1) return s;
		return s.charAt(0).toUpperCase() + s.substr(1).toLowerCase();
	}

	singularise(s) {
		if (/ies$/.test(s))
			return s.replace(/ies$/,'y');
		return s.replace(/s$/,'');
	}
	
	alignLeft(s, x=16) {
		var g = x - s.length, ss = [];
		for (var i=0;i<g;i++) ss.push(' ');
		return ss.join('');
	}

	/**
	 * Build a string like '"app_id":1,"name":1,...' for creating a golang map.
	 * if forput==true, exclude fields with editable==false
	 */
	makeFieldsMap(forput?: boolean) {
		var mp = [];
		for (var f of this.properties) {
			if (forput) {
				if (f.editable) {
					mp.push(`"${f.columnName}":1`)
				}
			} else if (f.columnName != 'created_at') {
				mp.push(`"${f.columnName}":1`)
			}
		}
		return mp.join(',');
	}
	
	/**
	 * Build a string like '&e.Id,&e.Username,..' for passing to sql Rows.Scan.
	 */
	makeScanParams() {
		var sps = [];
		for (var f of this.properties) {
			sps.push(`&e.${f.capitalName}`);
		}
		return sps.join(',');
	}

	columnNames() {
		var cs = [];
		for (var f of this.properties) {
			cs.push(f.columnName);
		}
		return cs;
	}

	varcharMap() {
		var vs = [];
		for (var f of this.properties) {
			if (f.columnName == 'id') continue;
			if (f.columnName == 'created_at') continue;
			if (f.generator) continue;
			var v = (f.dataType == 'zero.String') ? 0 : 1;
			vs.push(`"${f.columnName}":${v}`);
		}
		return vs.join(',');
	}

	attributeLines(pfx) {
		var als = [];
		for (var f of this.properties) {
			als.push(`${pfx}${f.capitalName}: ${f.dataType}`)
		}
		return als;
	}
}

/**
 * SchemaFiles
 */
export class SchemaFiles {
	schema_dir: string;
	schemas: SchemaEntity[];
	
	constructor(schema_dir) {
		this.schema_dir = schema_dir;
		this.schemas = [];
	}
	
	/**
	 * @cb : callback(schema_filename)
	 * @save: set to true if need saving each schema in this.schemas array.
	 */
	traverse(cb, save=false) {
		var schema;
		for (var fn of fs.readdirSync(this.schema_dir)) {
			if (cb) {
				schema = cb(fn);
			} else {
				schema = new SchemaEntity(this.schema_dir, fn);
			}
			if (save) {
				this.schemas.push(schema);
			}
		}
	}
}
