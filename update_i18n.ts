///<reference path="typings/node/node.d.ts" />
import fs = require('fs')

/**
 * I18nBundler
 */
class I18nBundler {
	msg_path: string
	all_json: JSON
	by_lang: {}
	
	constructor(i18n_path) {
		this.msg_path = i18n_path
		let fname = i18n_path + '/all.json'
		try {
			this.all_json = JSON.parse(fs.readFileSync(fname,'utf8'))
		} catch (e) {
			console.log(e)
		}
	}

	//by_lang[lang][id]=translation	
	put(lang, id, translation) {
		if (!this.by_lang[lang]) {
			this.by_lang[lang] = {}
		}
		this.by_lang[lang][id] = translation
	}
	
	// e: {id:'KEY', en:'..', fr:'..', ...}
	put_entry(e) {
		let id = e.id 
		for (var p in e) {
			if (p != 'id') {
				this.put(p, id, e[p]);
			}
		}
	}
	
	save_by_lang() {
		for (var lang in this.by_lang) {
			let fname = `${this.msg_path}/${lang}.json`
			fs.writeFileSync(fname, JSON.stringify(this.by_lang[lang]))
		}
	}
	
	distribute() {
		this.by_lang = {}
		for (var k in this.all_json) {
			var e = this.all_json[k]
			this.put_entry(e)
		}
		this.save_by_lang()
	}
}

new I18nBundler('../i18n/msgs').distribute()
