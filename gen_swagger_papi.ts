/// <references path="typings/node/node.d.ts" />
import fs = require('fs')
import Loader = require('./SchemaLoader')

class SchemaFile extends Loader.SchemaEntity {
    constructor(schema_dir, schema_filename) {
        super(schema_dir, schema_filename)
    }
    
    cleaned() {
        var oschema = {}
        for (var k in this.schema) {
            if (k == 'properties') {
                oschema['properties'] = {}
                for (var p in this.schema[k]) {
                    oschema['properties'][p] = {}
                    for (var pi in this.schema[k][p]) {
                        if (pi.indexOf('_') < 0) {
                            oschema['properties'][p][pi] = this.schema[k][p][pi];
                        }
                    }
                }
            } else if (['$schema','required','additionalProperties','_routes'].indexOf(k) < 0) {
                oschema[k] = this.schema[k]
            }
        }
        return oschema
    }
    
    endpoints(paths) {
        var c = '/' + this.tableName,
            s = c + '/{id}'
        paths[c] = {
            get: {
                description: `Return all ${this.tableName}`,
                responses: {
                    '200': {
                        description: `A list of ${this.tableName}`,
                        schema: {
                            title: this.singularName,
                            type: 'array',
                            items: {
                                '$ref': `#/definitions/${this.singularName}`
                            }
                        }
                    }
                },
                security: [
                    {
                        apikey: []
                    },
                    {
                        jwt: []
                    }
                ]
            },
            post: {
                parameters: [
                    {
                        name: this.singularise(this.singularName),
                        in: 'body',
                        description: `Create a new ${this.singularName}`,
                        schema: {
                            '$ref': `#/definitions/${this.singularName}`
                        },
                        required: true
                    }
                ],
                responses: {
                    '201': {
                        description: `A new ${this.singularName} created`
                    }
                },
                security: [
                    {
                        apikey: []
                    },
                    {
                        jwt: []
                    }
                ]
            }
        }
        paths[s] = {
            get: {
                parameters: [
                    {
                        name: 'id',
                        in: 'path',
                        type: 'string',
                        description: 'ID',
                        required: true
                    }
                ],
                responses: {
                    '200': {
                        description: `Return ${this.singularName} by ID`
                    }
                },
                security: [
                    {
                        apikey: []
                    }
                ]
            },
            put: {
                parameters: [
                    {
                        in: 'path',
                        name: 'id',
                        required: true,
                        type: 'string'
                    },
                    {
                        in: 'body',
                        name: 'body',
                        description: `Update ${this.singularName}`,
                        required: true,
                        schema: {
                            '$ref': `#/definitions/${this.singularName}`
                        }
                    }
                ],
                responses: {
                    '200': {
                        description: 'Updated '+this.singularName
                    },
                    '404': {
                        description: this.singularName + ' not found'
                    }
                },
                security: [
                    {
                        apikey: []
                    },
                    {
                        jwt: []
                    }
                ]
            }
        }
    }
}

const SCHEMA_DIR = __dirname + '/../schemas/'
const TITLE = 'MakeitSocial Product API v2'
const DESC = 'The Make it Social Product API version 2'
const TERMS = 'https://makeitsocial.com/terms'
const VERSION = '0.1.0'

const ENTITIES = ['addons','allocations','apps','bookings','captures','discounts','fees',
    'generics','instances','managers','prices','products','producttypes','providers','bookingengines',
    'requests','rrules','seatmaps','tags','tickets','times','venues','vouchers','xids']

/**
 * Swagger
 */
class Swagger {
    outdir: string
    swagger: Object
    
    constructor(outdir) {
        this.outdir = outdir
        this.swagger = {
            swagger: '2.0',
            info: {
                title: TITLE,
                description: DESC,
                termsOfService: TERMS,
                version: VERSION,
                contact: {
                    name: 'MakeitSocial',
                    url: 'https://makeitsocial.com',
                    email: 'dev@makeitsocial.com'
                }
            },
            basePath: '/v2',
            schemes: 'https',
            consumes: ['application/json'],
            produces: ['application/json'],
            securityDefinitions: {
                "apikey": {
                    "type": "apiKey",
                    "name": "X-Api-Key",
                    "in": "header"
                },
                jwt: {
                    type: 'apiKey',
                    name: 'Authorization',
                    in: 'header',
                    description: 'Token authentication with JWT'
                }
            },
            paths: {},
            definitions: {}
        }
    }
    
    run() {
        this.swagger['paths'] = gen_paths_from_routes();
        new Loader.SchemaFiles(SCHEMA_DIR).traverse((schema_filename)=>{
            //get dataset, remove _ props
            var s = new SchemaFile(SCHEMA_DIR, schema_filename)
            if (ENTITIES.indexOf(s.tableName) < 0) {
                return
            }
            this.swagger['definitions'][s.singularName] = s.cleaned()
            // s.endpoints(this.swagger['paths']) //use new endpoints generator from routes
        })
        //save this.swagger file
        fs.writeFileSync(this.outdir+'swagger.json', JSON.stringify(this.swagger,null,4))
        //console.log(JSON.stringify(this.swagger,null,4))
    }
}

// new: collect endpoints from g_router.go generated by gen_routes_papi.ts
function gen_paths_from_routes() {
    var eps = {};
    var lines = fs.readFileSync('../api/g_router.go','utf-8').split('\n');
    for (var line of lines) {
        var tableName, structName;
        if (/^\s+\/\/\w+ - (\w+)\s*$/.test(line)) {
            var m = /^\s+\/\/(\w+) - (\w+)\s*$/.exec(line);
            tableName = m[1];
            structName = m[2]
        }
        if (/^\s+r\./.test(line)) {
            var method = line.substring(line.indexOf('.')+1,line.indexOf('(')).toLowerCase();
            var paths = line.substring(line.indexOf('"')+1,line.indexOf('",')).replace(/:(\w+)/g,'{$1}');
            var noJwtAuth = line.indexOf('WrapCall') > 0;
            var create = line.indexOf('(create') > 0;
            var queryAll = line.indexOf(`(query${structName}s`) > 0;
            var queryOne = line.indexOf(`(query${structName})`) > 0;
            if (!eps[paths]) eps[paths] = {};
            eps[paths][method] = {
                parameters: [],
                responses: {},
                security: [
                    {
                        apikey: []
                    }
                ]
            };
            var e = eps[paths][method];
            if (!noJwtAuth) {
                e['security'].push({jwt:[]});
            }
            var rg = /\{\w+\}/gi, r;
            while ((r=rg.exec(paths))) {
                e['parameters'].push({
                    name: r[0].replace(/[{}]/g,''),
                    in: 'path',
                    type: 'string',
                    required: true
                })
            }
            if (create) {
                e['responses']['201'] = {}
            } else if (method == 'delete') {
                e['responses']['204'] = {}
            } else {
                e['responses']['200'] = {}
            }
            if (method == 'get') {
                if (queryAll) e['description'] = `Return all ${tableName}`;
                if (queryOne) e['description'] = `Return one ${structName}`;
                if (queryAll) {
                    e['responses']['200']['schema'] = {
                        title: tableName,
                        type: 'array',
                        items: {
                            '$ref': `#/definitions/${structName}`
                        }
                    }
                } else if (queryOne) {
                    e['responses']['200']['schema'] = {
                        '$ref': `#/definitions/${structName}`
                    }
                }
            } else if (method != 'delete') {
                e['parameters'].push({
                    name: structName,
                    in: 'body',
                    required: true,
                    schema: {
                        '$ref': `#/definitions/${structName}`
                    }
                })
            }
            if (e['parameters'].length < 1) {
                delete e['parameters'];
            }
        }
    }
    return eps;
}

const OUT_DIR = __dirname + '/../public/'

new Swagger(OUT_DIR).run()
