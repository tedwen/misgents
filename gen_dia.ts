/// <reference path="typings/node/node.d.ts" />
import fs = require('fs');
import Loader = require('./SchemaLoader');

// This script generates both ER and UML class diagrams for the JSON schemas.
// The output diagrams are in GraphML language to be formatted by yEd.
// Use yEd to open the .graphml file, and select Layout->Hierarchical to layout the diagram.

const PROJECT_NAME = process.argv[2] || '',
	  PARENT_DIR = __dirname.substring(0, __dirname.lastIndexOf('/')),
	  PROJECT_DIR = PROJECT_NAME ? PARENT_DIR + '/' + PROJECT_NAME : PARENT_DIR,
	  SCHEMA_DIR = PROJECT_DIR + '/schemas/',
	  OUTPUT_DIR = PROJECT_DIR + '/docs/diagrams/';

const yed_header = `<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<graphml xmlns="http://graphml.graphdrawing.org/xmlns" xmlns:java="http://www.yworks.com/xml/yfiles-common/1.0/java" xmlns:sys="http://www.yworks.com/xml/yfiles-common/markup/primitives/2.0" xmlns:x="http://www.yworks.com/xml/yfiles-common/markup/2.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:y="http://www.yworks.com/xml/graphml" xmlns:yed="http://www.yworks.com/xml/yed/3" xsi:schemaLocation="http://graphml.graphdrawing.org/xmlns http://www.yworks.com/xml/schema/graphml/1.1/ygraphml.xsd">
  <!--Created by yEd 3.16.2.1-->
    <key attr.name="Description" attr.type="string" for="graph" id="d0"/>
  <key for="port" id="d1" yfiles.type="portgraphics"/>
  <key for="port" id="d2" yfiles.type="portgeometry"/>
  <key for="port" id="d3" yfiles.type="portuserdata"/>
  <key attr.name="url" attr.type="string" for="node" id="d4"/>
  <key attr.name="description" attr.type="string" for="node" id="d5"/>
  <key for="node" id="d6" yfiles.type="nodegraphics"/>
  <key for="graphml" id="d7" yfiles.type="resources"/>
  <key attr.name="url" attr.type="string" for="edge" id="d8"/>
  <key attr.name="description" attr.type="string" for="edge" id="d9"/>
  <key for="edge" id="d10" yfiles.type="edgegraphics"/>
  <graph edgedefault="directed" id="G">
    <data key="d0"/>
    __NODES__
    __EDGES__
  </graph>
  <data key="d7">
    <y:Resources/>
  </data>
</graphml>
`;

const er_node_template = `
  <node id="n__N__">
    <data key="d5"/>
    <data key="d6">
        <y:GenericNode configuration="com.yworks.entityRelationship.big_entity">
          <y:Geometry height="90.0" width="80.0" x="186.0" y="17.0"/>
          <y:Fill color="#E8EEF7" color2="#B7C9E3" transparent="false"/>
          <y:BorderStyle color="#000000" type="line" width="1.0"/>
          <y:NodeLabel alignment="center" autoSizePolicy="content" backgroundColor="#B7C9E3" configuration="com.yworks.entityRelationship.label.name" fontFamily="Dialog" fontSize="12" fontStyle="plain" hasLineColor="false" height="18.1328125" horizontalTextPosition="center" iconTextGap="4" modelName="internal" modelPosition="t" textColor="#000000" verticalTextPosition="bottom" visible="true" width="48.548828125" x="15.7255859375" y="4.0">__TABLENAME__</y:NodeLabel>
          <y:NodeLabel alignment="left" autoSizePolicy="content" configuration="com.yworks.entityRelationship.label.attributes" fontFamily="Dialog" fontSize="12" fontStyle="plain" hasBackgroundColor="false" hasLineColor="false" height="46.3984375" horizontalTextPosition="center" iconTextGap="4" modelName="custom" textColor="#000000" verticalTextPosition="top" visible="true" width="65.541015625" x="2.0" y="30.1328125">__COLUMNS__<y:LabelModel>
              <y:ErdAttributesNodeLabelModel/>
            </y:LabelModel>
            <y:ModelParameter>
              <y:ErdAttributesNodeLabelModelParameter/>
            </y:ModelParameter>
          </y:NodeLabel>
          <y:StyleProperties>
            <y:Property class="java.lang.Boolean" name="y.view.ShadowNodePainter.SHADOW_PAINTING" value="true"/>
          </y:StyleProperties>
        </y:GenericNode>
    </data>
  </node>
`;
const er_edge_template = `
  <edge id="e__N__" source="n__S__" target="n__T__">
    <data key="d0"/>
    <data key="d10">
        <y:PolyLineEdge>
          <y:Path sx="0.0" sy="0.0" tx="0.0" ty="0.0"/>
          <y:LineStyle color="#000000" type="line" width="1.0"/>
          <y:Arrows source="__ETYPE__" target="none"/>
          <y:BendStyle smoothed="false"/>
        </y:PolyLineEdge>
    </data>
  </edge>
`;
const class_node_template = `
  <node id="n__N__">
    <data key="d4"/>
    <data key="d6">
        <y:UMLClassNode>
          <y:Geometry height="120.0" width="100.0" x="17.0" y="52.0"/>
          <y:Fill color="#FFCC00" transparent="false"/>
          <y:BorderStyle color="#000000" type="line" width="1.0"/>
          <y:NodeLabel alignment="center" autoSizePolicy="content" fontFamily="Dialog" fontSize="13" fontStyle="bold" hasBackgroundColor="false" hasLineColor="false" height="19.310546875" horizontalTextPosition="center" iconTextGap="4" modelName="custom" textColor="#000000" verticalTextPosition="bottom" visible="true" width="60.265625" x="19.8671875" y="3.0">__CLASSNAME__<y:LabelModel>
              <y:SmartNodeLabelModel distance="4.0"/>
            </y:LabelModel>
            <y:ModelParameter>
              <y:SmartNodeLabelModelParameter labelRatioX="0.0" labelRatioY="0.0" nodeRatioX="0.0" nodeRatioY="-0.03703090122767855" offsetX="0.0" offsetY="0.0" upX="0.0" upY="-1.0"/>
            </y:ModelParameter>
          </y:NodeLabel>
          <y:UML clipContent="true" constraint="" omitDetails="false" stereotype="" use3DEffect="true">
            <y:AttributeLabel>__ATTRIBS__</y:AttributeLabel>
            <y:MethodLabel></y:MethodLabel>
          </y:UML>
        </y:UMLClassNode>
    </data>
  </node>
`;

class DiagramEntity extends Loader.SchemaEntity {
    idx: number;
	constructor(schema_dir, schema_filename, idx) {
		super(schema_dir, schema_filename);
        this.idx = idx;
	}

    gen_er_node() {
        let ps = (this.properties.length - 4) * 14, h = ps + 95;
        let wn = Math.max(11, this.tableName.length);
        for (let p of this.properties) {
            let pn = p.columnName.length;
            if (pn > wn) wn = pn;
        }
        let s = er_node_template.replace('__N__', this.idx.toString())
            .replace('__TABLENAME__', this.tableName)
            .replace('__COLUMNS__', this.columnNames().join('\n'))
            .replace('height="90.0"', `height="${h}.0"`);
        if (wn > 11) {
            let w = 80 + 10 * (wn - 11);
            s = s.replace('width="80.0"', `width="${w}.0"`);
        }
        return s;
    }

    gen_class_node() {
        let ps = (this.properties.length - 4) * 14, h = ps + 120;
        let wn = Math.max(11, this.tableName.length);
        for (let p of this.properties) {
            let pn = p.columnName.length;
            if (pn > wn) wn = pn;
        }
        let s = class_node_template.replace('__N__', this.idx.toString())
            .replace('__CLASSNAME__', this.structName)
            .replace('__ATTRIBS__', this.attributeLines('-').join('\n'))
            .replace('height="120.0"', `height="${h}.0"`);
        if (wn > 11) {
            let w = 100 + 10 * (wn - 11);
            s = s.replace('width="100.0"', `width="${w}.0"`);
        }
        return s;
    }
}

class Diagrammer {
    output_dir: string;
    er_filename: string;
    class_filename: string;

    constructor(outdir) {
        this.output_dir = outdir;
        this.er_filename = `${this.output_dir}papi-ER.graphml`;
        this.class_filename = `${this.output_dir}papi-class.graphml`;
    }

    gen_edge(idx, ns, nt, et) {
        let s = er_edge_template.replace('__N__', idx)
            .replace('__S__', ns).replace('__T__', nt);
        return s.replace('__ETYPE__', et);
    }

    gen_edges(entities, nodemap, etype) {
        let buf = [], x = 0;
        for (let e of entities) {
            for (let p of e.properties) {
                let fk = p.foreignKeyTable();
                if (fk) {
                    let nt = nodemap[fk];
                    let ns = nodemap[e.tableName];
                    buf.push(this.gen_edge(x,ns,nt,etype));
                    x ++;
                }
            }
        }
        return buf.join('\n');
    }

    save_er(nodes, edges) {
        let buf = yed_header.replace('__NODES__', nodes)
            .replace('__EDGES__', edges);
        fs.writeFileSync(this.er_filename, buf, {encoding:'utf-8'});
    }

    save_class(cnodes, cedges) {
        let buf = yed_header.replace('__NODES__', cnodes)
            .replace('__EDGES__', cedges);
        fs.writeFileSync(this.class_filename, buf, {encoding:'utf-8'});
    }

    generate() {
        let nodex = 0;
        let nodes = [], cnodes = [];
        let nodemap = {};
        let entities = [];
        new Loader.SchemaFiles(SCHEMA_DIR).traverse(function(schema_filename){
            let dia = new DiagramEntity(SCHEMA_DIR, schema_filename, nodex);
            let er = dia.gen_er_node();
            nodes.push(er);
            let cs = dia.gen_class_node();
            cnodes.push(cs);
            entities.push(dia);
            nodemap[dia.tableName] = nodex;
            // let klass = dia.gen_class_node();
            console.log(`${nodex}:${dia.tableName}`);
            nodex ++;
        });
        let edges = this.gen_edges(entities, nodemap, 'crows_foot_many_optional');
        this.save_er(nodes.join('\n'), edges);
        let cedges = this.gen_edges(entities, nodemap, 'white_diamond');
        this.save_class(cnodes.join('\n'), cedges);
        console.log('All done');
    }
}

new Diagrammer(OUTPUT_DIR).generate();
